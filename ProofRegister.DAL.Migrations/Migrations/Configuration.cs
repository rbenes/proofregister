using System.Data.Entity.Migrations;

namespace ProofRegister.DAL.Migrations.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ProofRegisterDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;

            // Kdybych nenastavil ContextKey, vlozila by se do __MigrationHistory
            // hodnota KB.SecuritiesRegister.DAL.Migrations.Migrations.Configuration"
            // a nebylo by mozne v konstruktoru SecuritiesRegisterDbContext volat
            // pro kontrolu modelu this.Database.CompatibleWithModel()
            ContextKey = typeof(ProofRegisterDbContext).FullName;
        }

        protected override void Seed(ProofRegisterDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
