namespace ProofRegister.DAL.Migrations.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "PR.ProjectPeriods",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProjectId = c.Long(nullable: false),
                        Name = c.String(nullable: false, maxLength: 128),
                        ValidFrom = c.DateTime(),
                        ValidTo = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        CreatedWhen = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        ModifiedWhen = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("PR.Projects", t => t.ProjectId)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "PR.Projects",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        ValidFrom = c.DateTime(),
                        ValidTo = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        CreatedWhen = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        ModifiedWhen = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "PR.TrackingDevices",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        KudyId = c.String(nullable: false, maxLength: 128),
                        Enabled = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 128),
                        CreatedWhen = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        ModifiedWhen = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ProjectDal_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("PR.Projects", t => t.ProjectDal_Id)
                .Index(t => t.ProjectDal_Id);
            
            CreateTable(
                "PR.ProjectPointDals",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProjectId = c.Long(nullable: false),
                        Name = c.String(nullable: false, maxLength: 128),
                        Position_Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Position_Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Radius = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProjectPointTypeId = c.Long(nullable: false),
                        Address_Street = c.String(maxLength: 128),
                        Address_City = c.String(maxLength: 128),
                        Address_PostCode = c.String(maxLength: 10),
                        CreatedBy = c.String(maxLength: 128),
                        CreatedWhen = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        ModifiedWhen = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("PR.Projects", t => t.ProjectId)
                .ForeignKey("PR.ProjectPointTypes", t => t.ProjectPointTypeId)
                .Index(t => t.ProjectId)
                .Index(t => t.ProjectPointTypeId);
            
            CreateTable(
                "PR.ProjectPointTypes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProjectId = c.Long(nullable: false),
                        Name = c.String(nullable: false, maxLength: 128),
                        CreatedBy = c.String(maxLength: 128),
                        CreatedWhen = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        ModifiedWhen = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("PR.Projects", t => t.ProjectId)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "PR.ProjectPointVisits",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProjectPointId = c.Long(nullable: false),
                        ProjectPeriodId = c.Long(nullable: false),
                        TrackId = c.Long(nullable: false),
                        RealTrackInfo_EntryTime = c.DateTime(nullable: false),
                        RealTrackInfo_EntryPosition_Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RealTrackInfo_EntryPosition_Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RealTrackInfo_ExitTime = c.DateTime(nullable: false),
                        RealTrackInfo_ExitPosition_Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RealTrackInfo_ExitPosition_Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProtocolInfo_EntryTime = c.DateTime(nullable: false),
                        ProtocolInfo_EntryPosition_Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProtocolInfo_EntryPosition_Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProtocolInfo_ExitTime = c.DateTime(nullable: false),
                        ProtocolInfo_ExitPosition_Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProtocolInfo_ExitPosition_Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProjectPointInfo = c.String(),
                        CreatedBy = c.String(maxLength: 128),
                        CreatedWhen = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        ModifiedWhen = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("PR.ProjectPeriods", t => t.ProjectPeriodId)
                .ForeignKey("PR.ProjectPointDals", t => t.ProjectPeriodId)
                .ForeignKey("PR.Tracks", t => t.TrackId)
                .Index(t => t.ProjectPeriodId)
                .Index(t => t.TrackId);
            
            CreateTable(
                "PR.Tracks",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ImportTime = c.DateTime(nullable: false),
                        TrackStatusId = c.Long(nullable: false),
                        Distance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedBy = c.String(maxLength: 128),
                        CreatedWhen = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        ModifiedWhen = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("PR.TrackStatuses", t => t.TrackStatusId)
                .Index(t => t.TrackStatusId);
            
            CreateTable(
                "PR.TrackSegments",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TrackId = c.Long(nullable: false),
                        Position_Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Position_Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Elevation = c.Decimal(precision: 18, scale: 2),
                        SegmentTime = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 128),
                        CreatedWhen = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        ModifiedWhen = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("PR.Tracks", t => t.TrackId)
                .Index(t => t.TrackId);
            
            CreateTable(
                "PR.TrackStatuses",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(maxLength: 256),
                        CreatedBy = c.String(maxLength: 128),
                        CreatedWhen = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        ModifiedWhen = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "PR.NielsenShopProjectPoints",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Ico = c.String(maxLength: 128),
                        Dic = c.String(maxLength: 128),
                        IdNielsen = c.String(maxLength: 128),
                        StoreCode = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("PR.ProjectPointDals", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("PR.NielsenShopProjectPoints", "Id", "PR.ProjectPointDals");
            DropForeignKey("PR.ProjectPointVisits", "TrackId", "PR.Tracks");
            DropForeignKey("PR.Tracks", "TrackStatusId", "PR.TrackStatuses");
            DropForeignKey("PR.TrackSegments", "TrackId", "PR.Tracks");
            DropForeignKey("PR.ProjectPointVisits", "ProjectPeriodId", "PR.ProjectPointDals");
            DropForeignKey("PR.ProjectPointVisits", "ProjectPeriodId", "PR.ProjectPeriods");
            DropForeignKey("PR.ProjectPointDals", "ProjectPointTypeId", "PR.ProjectPointTypes");
            DropForeignKey("PR.ProjectPointTypes", "ProjectId", "PR.Projects");
            DropForeignKey("PR.ProjectPointDals", "ProjectId", "PR.Projects");
            DropForeignKey("PR.ProjectPeriods", "ProjectId", "PR.Projects");
            DropForeignKey("PR.TrackingDevices", "ProjectDal_Id", "PR.Projects");
            DropIndex("PR.NielsenShopProjectPoints", new[] { "Id" });
            DropIndex("PR.TrackSegments", new[] { "TrackId" });
            DropIndex("PR.Tracks", new[] { "TrackStatusId" });
            DropIndex("PR.ProjectPointVisits", new[] { "TrackId" });
            DropIndex("PR.ProjectPointVisits", new[] { "ProjectPeriodId" });
            DropIndex("PR.ProjectPointTypes", new[] { "ProjectId" });
            DropIndex("PR.ProjectPointDals", new[] { "ProjectPointTypeId" });
            DropIndex("PR.ProjectPointDals", new[] { "ProjectId" });
            DropIndex("PR.TrackingDevices", new[] { "ProjectDal_Id" });
            DropIndex("PR.ProjectPeriods", new[] { "ProjectId" });
            DropTable("PR.NielsenShopProjectPoints");
            DropTable("PR.TrackStatuses");
            DropTable("PR.TrackSegments");
            DropTable("PR.Tracks");
            DropTable("PR.ProjectPointVisits");
            DropTable("PR.ProjectPointTypes");
            DropTable("PR.ProjectPointDals");
            DropTable("PR.TrackingDevices");
            DropTable("PR.Projects");
            DropTable("PR.ProjectPeriods");
        }
    }
}
