﻿using System;
using System.Collections.Generic;

namespace ProofRegister.Contracts.Entities
{
    public class ProjectDto : EntityBaseDto
    {
        public ProjectDto()
        {
            Periods = new List<ProjectPeriodDto>();
            Points = new List<ProjectPointDto>();
            Devices = new List<TrackingDeviceDto>();
        }

        public string Name { get; set; }

        public DateTime? ValidFrom { get; set; }

        public DateTime? ValidTo { get; set; }

        public ICollection<ProjectPeriodDto> Periods { get; set; }

        public ICollection<ProjectPointDto> Points { get; set; }

        public ICollection<TrackingDeviceDto> Devices { get; set; }
    }
}
