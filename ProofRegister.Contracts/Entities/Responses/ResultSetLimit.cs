﻿namespace ProofRegister.Contracts.Entities.Responses
{
    public class ResultSetLimit
    {
        public int Count { get; set; }
        
        public int? Page { get; set; }
        
        public int? PageSize { get; set; }
    }
}
