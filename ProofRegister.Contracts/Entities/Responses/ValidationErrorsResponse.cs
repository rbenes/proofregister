﻿using System.Collections.Generic;

namespace ProofRegister.Contracts.Entities.Responses
{
    /// <summary>
    /// Validation response.
    /// </summary>
    public class ValidationErrorsResponse
    {
        /// <summary>
        /// Collection of Validation messages.
        /// </summary>
        public ICollection<string> Messages { get; set; }
    }
}
