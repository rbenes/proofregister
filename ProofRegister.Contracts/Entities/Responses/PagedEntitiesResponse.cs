﻿using System.Collections.Generic;

namespace ProofRegister.Contracts.Entities.Responses
{
    public class PagedEntitiesResponse<T> where T : EntityBaseDto
    {
        public ResultSetLimit ResultSetLimit { get; set; }
        
        public IEnumerable<T> Entities { get; set; }
    }
}
