﻿using System;

namespace ProofRegister.Contracts.Entities
{
    public class ProjectPeriodDto : EntityBaseDto
    {
        public long ProjectId { get; set; }

        public ProjectDto Project { get; set; }

        public string Name { get; set; }

        public DateTime? ValidFrom { get; set; }

        public DateTime? ValidTo { get; set; }
    }
}
