﻿namespace ProofRegister.Contracts.Entities
{
    public class ProjectPointTypeDto : EntityBaseDto
    {
        public long ProjectId { get; set; }

        public ProjectDto Project { get; set; }

        public string Name { get; set; }
    }
}
