﻿namespace ProofRegister.Contracts.Entities
{
    public class TrackStatusDto : EntityBaseDto
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
