﻿using System;

namespace ProofRegister.Contracts.Entities
{
    public class TrackSegmentDto : EntityBaseDto
    {
        public TrackSegmentDto()
        {
            Position = new PositionDto();
        }

        public long TrackId { get; set; }

        public TrackDto Track { get; set; }

        public PositionDto Position { get; set; }

        public decimal? Elevation { get; set; }

        public DateTime SegmentTime { get; set; }
    }
}
