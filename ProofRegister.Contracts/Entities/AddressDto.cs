﻿namespace ProofRegister.Contracts.Entities
{
    public class AddressDto
    {
        public string Street { get; set; }

        public string City { get; set; }

        public string PostCode { get; set; }

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(Street)}: {Street}, {nameof(City)}: {City}, {nameof(PostCode)}: {PostCode}";
        }
    }
}
