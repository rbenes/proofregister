﻿namespace ProofRegister.Contracts.Entities
{
    public class ProjectPointVisitDto : EntityBaseDto
    {
        public ProjectPointVisitDto()
        {
            RealTrackInfo = new VisitInfoDto();
            ProtocolInfo = new VisitInfoDto();
        }

        public long ProjectPointId { get; set; }

        public ProjectPointDto ProjectPoint { get; set; }

        public long ProjectPeriodId { get; set; }

        public ProjectPeriodDto ProjectPeriod { get; set; }

        public long TrackId { get; set; }

        public TrackDto Track { get; set; }

        public VisitInfoDto RealTrackInfo { get; set; }

        public VisitInfoDto ProtocolInfo { get; set; }

        public string ProjectPointInfo { get; set; }
    }
}
