﻿namespace ProofRegister.Contracts.Entities
{
    public class ProjectPointDto : EntityBaseDto
    {
        public long ProjectId { get; set; }

        public ProjectDto Project { get; set; }

        public string Name { get; set; }

        public PositionDto Position { get; set; }

        public decimal Radius { get; set; }

        public ProjectPointTypeDto ProjectPointType { get; set; }

        public long ProjectPointTypeId { get; set; }

        public AddressDto Address { get; set; }

        protected ProjectPointDto()
        {
            Position = new PositionDto();
            Address = new AddressDto();
        }
    }
}
