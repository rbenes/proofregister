﻿namespace ProofRegister.Contracts.Entities
{
    public class TrackingDeviceDto : EntityBaseDto
    {
        public string KudyId { get; set; }

        public bool Enabled { get; set; }
    }
}
