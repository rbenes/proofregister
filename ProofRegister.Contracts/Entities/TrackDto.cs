﻿using System;
using System.Collections.Generic;

namespace ProofRegister.Contracts.Entities
{
    public class TrackDto : EntityBaseDto
    {
        public TrackDto()
        {
            Segments = new List<TrackSegmentDto>();
        }

        public DateTime ImportTime { get; set; }

        public TrackStatusDto TrackStatus { get; set; }

        public long TrackStatusId { get; set; }

        public decimal Distance { get; set; }

        public ICollection<TrackSegmentDto> Segments { get; set; }
    }
}
