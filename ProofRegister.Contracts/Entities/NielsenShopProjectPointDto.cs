﻿namespace ProofRegister.Contracts.Entities
{
    public class NielsenShopProjectPointDto : ProjectPointDto
    {
        public string Ico { get; set; }

        public string Dic { get; set; }

        public string IdNielsen { get; set; }

        public string StoreCode { get; set; }
    }
}
