﻿namespace ProofRegister.Contracts.Entities
{
    public class PositionDto
    {
        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }
    }
}
