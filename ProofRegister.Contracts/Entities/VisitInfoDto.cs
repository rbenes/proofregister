﻿using System;

namespace ProofRegister.Contracts.Entities
{
    public class VisitInfoDto
    {
        public VisitInfoDto()
        {
            EntryPosition = new PositionDto();
            ExitPosition = new PositionDto();
        }

        public DateTime EntryTime { get; set; }

        public PositionDto EntryPosition { get; set; }

        public DateTime ExitTime { get; set; }

        public PositionDto ExitPosition { get; set; }
    }
}
