﻿using System;
using ProofRegister.Contracts.Entities;

namespace ProofRegister.Contracts
{
    public static class WebApiCommon
    {
        public static string GetServiceName(Type typ)
        {
            if (typ == typeof(TrackingDeviceDto))
                return "tracking-devices";

            if (typ == typeof(ProjectDto))
                return "projects";

            if (typ == typeof(TrackDto))
                return "tracks";

            if (typ == typeof(TrackSegmentDto))
                return "track-segments";

            throw new NotSupportedException($"Type '{typ}' is not supported.");
        }

        public static string GetServiceName<T>()
        {
            return GetServiceName(typeof(T));
        }
    }
}
