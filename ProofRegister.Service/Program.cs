﻿using System;
using System.Web.Http.Dependencies;
using AutoMapper;
using ProofRegister.Common;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Service.Business.Communication;
using ProofRegister.Service.Business.Factories;
using ProofRegister.Service.Common;
using ProofRegister.Service.Infrastructure.Communication;
using ProofRegister.Service.Infrastructure.Factories;
using ProofRegister.Service.Infrastructure.Hosting;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;

namespace ProofRegister.Service
{
    internal class Program
    {
        // ReSharper disable once UnusedParameter.Local
        private static void Main(string[] args)
        {
            using (var container = new Container())
            {
                var webApiDependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);

                var config = new MapperConfiguration(cfg => { cfg.AddProfile<ProofRegisterServiceProfile>(); });

                config.AssertConfigurationIsValid();

                container.RegisterSingleton<ILogger, Logger>();
                container.RegisterSingleton<IExceptionHandling, ExceptionHandling>();
                container.Register<ISettings, Settings>();
                container.Register<IWebApiModule, WebApiModule>();
                container.Register<IWebApiConfigurator, WebApiConfigurator>();
                container.Register<IQueryProcessor, QueryProcessor>();
                container.Register<ICommandProcessor, CommandProcessor>();
                container.Register(typeof(ICommandHandler<>), new[] { typeof(Program).Assembly });
                container.Register(typeof(IQueryHandler<,>), new[] { typeof(Program).Assembly });
                container.Register<IDataServiceFactory, DataServiceFactory>();
                container.Register<IUnitOfWorkFactory, UnitOfWorkFactory>();
                container.Register<IRootHost, RootHost>();
                container.RegisterSingleton<IDependencyResolver>(() => webApiDependencyResolver);
                container.RegisterSingleton(config);
                // ReSharper disable once AccessToDisposedClosure
                container.Register(() => config.CreateMapper(container.GetInstance));

                container.Verify();

                var rootHost = container.GetInstance<IRootHost>();
                rootHost.Start();

                Console.ReadLine();

                rootHost.Stop();
            }
        }
    }
}
