﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using ProofRegister.Contracts.Entities;

namespace ProofRegister.Service.Infrastructure.WebApi
{
    public interface IApiControllerBase<TDtoEntity> where TDtoEntity : EntityBaseDto
    {
        Task<IHttpActionResult> Create([FromBody] TDtoEntity entity);
        Task<IHttpActionResult> Delete(long id);
        Task<IHttpActionResult> Get(int? page = null, int? pageSize = null);
        Task<IHttpActionResult> Get(long id);
        Task<IHttpActionResult> Update([FromBody] TDtoEntity entity);
    }
}
