﻿using ProofRegister.Service.Infrastructure.DAL;

namespace ProofRegister.Service.Infrastructure.Factories
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork GetUnitOfWork();
    }
}
