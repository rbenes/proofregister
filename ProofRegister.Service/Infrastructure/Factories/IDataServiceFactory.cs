﻿using ProofRegister.DAL.Entities;
using ProofRegister.Service.Infrastructure.Services;

namespace ProofRegister.Service.Infrastructure.Factories
{
    public interface IDataServiceFactory
    {
        IDataService<TDalEntity> GetDataService<TDalEntity>()
            where TDalEntity : EntityBaseDal;
    }
}
