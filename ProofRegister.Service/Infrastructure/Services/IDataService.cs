﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using ProofRegister.DAL.Entities;

namespace ProofRegister.Service.Infrastructure.Services
{
    public interface IDataService<TDalEntity>
        where TDalEntity : IEntityBaseDal
    {
        /// <summary>
        /// Vrati pocet polozek.
        /// </summary>
        /// <param name="filter">Filter.</param>
        /// <param name="cancellationToken">A CancellationToken to observe while waiting for the task to complete.</param>
        Task<int> CountAsync(Expression<Func<TDalEntity, bool>> filter, CancellationToken cancellationToken);

        /// <summary>
        /// Vytvori entitu.
        /// </summary>
        /// <param name="entity">Vytvarena entita.</param>
        /// <param name="cancellationToken">A CancellationToken to observe while waiting for the task to complete.</param>
        /// <param name="username"></param>
        Task<TDalEntity> CreateAsync(TDalEntity entity, CancellationToken cancellationToken, string username);

        /// <summary>
        /// Smaze entitu podle Id.
        /// </summary>
        /// <param name="id">Id entity.</param>
        /// <param name="cancellationToken">A CancellationToken to observe while waiting for the task to complete.</param>
        /// <returns>
        /// A task that represents the asynchronous delete operation.
        /// The task result will be false if the entity does not exist in the repository,
        /// or true if successfully removed.
        /// </returns>
        Task<bool> DeleteAsync(long id, CancellationToken cancellationToken);

        /// <summary>
        /// Smaze entitu.
        /// </summary>
        /// <param name="entity">Mazana entita.</param>
        /// <param name="cancellationToken">A CancellationToken to observe while waiting for the task to complete.</param>
        /// <returns>
        /// A task that represents the asynchronous delete operation.
        /// The task result will be false if the entity does not exist in the repository,
        /// or true if successfully removed.
        /// </returns>
        Task DeleteAsync(TDalEntity entity, CancellationToken cancellationToken);

        /// <summary>
        /// Vrati kolekci entit.
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="includes"></param>
        /// <param name="page">Cislo stranky (indexovano od nuly).</param>
        /// <param name="pageSize">Velikost stranky.</param>
        /// <param name="cancellationToken">A CancellationToken to observe while waiting for the task to complete.</param>
        Task<IEnumerable<TDalEntity>> GetsPagedAsync(
            Expression<Func<TDalEntity, bool>> filter,
            Func<IQueryable<TDalEntity>, IOrderedQueryable<TDalEntity>> orderBy,
            IEnumerable<Expression<Func<TDalEntity, object>>> includes,
            int? page,
            int? pageSize,
            CancellationToken cancellationToken);

        /// <summary>
        /// Updatuje entitu.
        /// </summary>
        /// <param name="entity">Updatovana entita.</param>
        /// <param name="cancellationToken">A CancellationToken to observe while waiting for the task to complete.</param>
        /// <param name="username">Username form Web.</param>
        Task<TDalEntity> UpdateAsync(TDalEntity entity, CancellationToken cancellationToken, string username);

        /// <summary>
        /// Updatuje entity.
        /// </summary>
        /// <param name="entities">Updatovane entity.</param>
        /// <param name="cancellationToken">A CancellationToken to observe while waiting for the task to complete.</param>
        /// <param name="username">Username form Web.</param>
        Task<IEnumerable<TDalEntity>> UpdateAsync(IEnumerable<TDalEntity> entities, CancellationToken cancellationToken, string username);
    }
}
