﻿using System.Net.Http;

namespace ProofRegister.Service.Infrastructure.Services
{
    public interface IRequestMetadataProvider
    {
        string GetUsername(HttpRequestMessage request);
    }
}
