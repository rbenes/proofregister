﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProofRegister.Service.Infrastructure.Events
{
    public interface IEventDispatcher
    {
        Task DispatchAsync(IEvent publishedEvent, CancellationToken cancellationToken);

        Task DispatchAndForget(IEvent publishedEvent, CancellationToken cancellationToken);

        void RegisterSubscriber<TEvent>(Func<Task> action) where TEvent : IEvent;

        void UnregisterSubscriber<TEvent>(Func<Task> action) where TEvent : IEvent;
    }
}
