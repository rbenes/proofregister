﻿using System;
using System.CodeDom;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProofRegister.Service.Infrastructure.Events
{
    public class EventDispatcher : IEventDispatcher
    {
        ConcurrentDictionary<Type, HashSet<Func<Task>>> subscribers = new ConcurrentDictionary<Type, HashSet<Func<Task>>>();

        public Task DispatchAsync(IEvent publishedEvent, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task DispatchAndForget(IEvent publishedEvent, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public void RegisterSubscriber<TEvent>(Func<Task> action) where TEvent : IEvent
        {
            var eventSubs = subscribers.GetOrAdd(typeof(TEvent), type => { return new HashSet<Func<Task>>();});
            eventSubs.Add(action);
        }

        public void UnregisterSubscriber<TEvent>(Func<Task> action) where TEvent : IEvent
        {
            throw new NotImplementedException();
        }
    }
}
