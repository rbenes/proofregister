﻿using System;
using System.Threading;
using System.Threading.Tasks;
using TrackableEntities;

namespace ProofRegister.Service.Infrastructure.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TEntity> GetRepository<TEntity>()
            where TEntity : class, ITrackable;

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
