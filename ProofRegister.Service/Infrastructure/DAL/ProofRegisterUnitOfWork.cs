﻿using System.Data.Entity;

namespace ProofRegister.Service.Infrastructure.DAL
{
    public class ProofRegisterUnitOfWork : UnitOfWork
    {
        public ProofRegisterUnitOfWork(DbContext dbContext)
            : base(dbContext)
        {
        }
    }
}
