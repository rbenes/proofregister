﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ProofRegister.Service.Infrastructure.DAL
{
    /// <summary>
    /// Generic repository interface with basic asynchronous operations.
    /// </summary>
    /// <typeparam name="TEntity">Entity type</typeparam>
    public interface IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Vrati pocet polozek dane entity na zaklade filtru.
        /// </summary>
        /// <param name="filter">Filter.</param>
        /// <param name="cancellationToken">A CancellationToken to observe while waiting for the task to complete.</param>
        /// <returns>Pocet polozek dane entity.</returns>
        Task<int> CountAsync(Expression<Func<TEntity, bool>> filter, CancellationToken cancellationToken);

        /// <summary>
        /// Removes an entity from the respository.
        /// </summary>
        /// <param name="cancellationToken">A CancellationToken to observe while waiting for the task to complete.</param>
        /// <param name="keyValues">The values of the primary key for the entity to be found.</param>
        /// <returns>A task that represents the asynchronous delete operation. The task result will be false if the entity does not exist in the repository, or true if successfully removed.</returns>
        Task<bool> DeleteAsync(CancellationToken cancellationToken, params long[] keyValues);

        /// <summary>
        /// Finds an entity with the given primary key values. If no entity is found, then null is returned.
        /// </summary>
        /// <param name="cancellationToken">A CancellationToken to observe while waiting for the task to complete.</param>
        /// <param name="keyValues">The values of the primary key for the entity to be found.</param>
        /// <returns>A task that represents the asynchronous find operation. The task result contains the entity found, or null.</returns>
        Task<TEntity> FindAsync(CancellationToken cancellationToken, params long[] keyValues);

        /// <summary>
        /// Vrátí všechny entity.
        /// </summary>
        /// <param name="cancellationToken">A CancellationToken to observe while waiting for the task to complete.</param>
        /// <returns>Všechny entity.</returns>
        Task<List<TEntity>> GetAllAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Load related entities for an object graph.
        /// </summary>
        /// <param name="entity">Entity on which related entities are loaded.</param>
        /// <param name="cancellationToken">A CancellationToken to observe while waiting for the task to complete.</param>
        /// <returns>A task that represents the asynchronous delete operation. The task result will be false if the entity does not exist in the repository, or true if successfully removed.</returns>
        Task LoadRelatedEntitiesAsync(TEntity entity, CancellationToken cancellationToken);

        /// <summary>
        /// Load related entities for more than one object graph.
        /// </summary>
        /// <param name="entities">Entities on which related entities are loaded.</param>
        /// <param name="cancellationToken">A CancellationToken to observe while waiting for the task to complete.</param>
        /// <returns>A task that represents the asynchronous delete operation. The task result will be false if the entity does not exist in the repository, or true if successfully removed.</returns>
        Task LoadRelatedEntitiesAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken);
        
        /// <summary>
        /// Inserts a new entity into the repository.
        /// </summary>
        /// <param name="entity">Entity to insert.</param>
        void Insert(TEntity entity);

        /// <summary>
        /// Inserts a new entities into the repository.
        /// </summary>
        /// <param name="entities">Entities to insert.</param>
        void Insert(IEnumerable<TEntity> entities);

        /// <summary>
        /// Updates an existing entity in the repository.
        /// </summary>
        /// <param name="entity">Entity to update.</param>
        void Update(TEntity entity);

        /// <summary>
        /// Updates an existing entities in the repository.
        /// </summary>
        /// <param name="entities">Entities to insert.</param>
        void Update(IEnumerable<TEntity> entities);

        /// <summary> The select async. </summary>
        /// <param name="filter"> The filter. </param>
        /// <param name="orderBy"> The order by. </param>
        /// <param name="includes"> The includes. </param>
        /// <param name="page"> The page. </param> 
        /// <param name="pageSize"> The page size. </param>
        /// <param name="cancellationToken"> The cancellation token. </param>
        /// <returns> The <see cref="Task"/>. </returns>
        Task<List<TEntity>> SelectAsync(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, List<Expression<Func<TEntity, object>>> includes = null, int? page = null, int? pageSize = null, CancellationToken? cancellationToken = null);
    }
}
