﻿using System;
using System.Collections.Concurrent;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Threading;
using System.Threading.Tasks;
using TrackableEntities;

namespace ProofRegister.Service.Infrastructure.DAL
{
    public abstract class UnitOfWork : IUnitOfWork
    {
        private bool isDisposed;
        protected ConcurrentDictionary<Type, object> Repositories = new ConcurrentDictionary<Type, object>();

        protected UnitOfWork(DbContext dbContext)
        {
            DbContext = dbContext;
        }

        protected DbContext DbContext { get; set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, ITrackable
        {
            return (IRepository<TEntity>) Repositories.GetOrAdd(typeof(TEntity), CreateRepository<TEntity>(DbContext));
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            if (isDisposed) throw new ObjectDisposedException(nameof(UnitOfWork));

            try
            {
                return await DbContext.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                var validEx = ex as DbEntityValidationException;
                if (validEx != null)
                    throw new Exception(validEx.Message, validEx);

                throw;
            }
        }

        protected virtual Repository<TEntity> CreateRepository<TEntity>(DbContext context)
            where TEntity : class, ITrackable
        {
            return new Repository<TEntity>(context);
        }

        /// <summary>
        ///     Disposes the DbContext.
        /// </summary>
        /// <param name="disposing">
        ///     True to release both managed and unmanaged resources; false to release only unmanaged
        ///     resources.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (isDisposed)
                return;

            if (disposing)
                DbContext.Dispose();

            isDisposed = true;
        }
    }
}
