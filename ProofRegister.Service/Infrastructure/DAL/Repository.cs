﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using LinqKit;
using ProofRegister.DAL.Entities;
using TrackableEntities;
using TrackableEntities.EF6;

namespace ProofRegister.Service.Infrastructure.DAL
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, ITrackable
    {
        public Repository(DbContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }

        protected DbContext Context { get; set; }
        protected DbSet<TEntity> Set => Context.Set<TEntity>();

        public async Task<int> CountAsync(Expression<Func<TEntity, bool>> filter, CancellationToken cancellationToken)
        {
            if (filter == null)
                return await Set.CountAsync(cancellationToken);

            return await Set.CountAsync(filter, cancellationToken);
        }

        public async Task<bool> DeleteAsync(CancellationToken cancellationToken, params long[] keyValues)
        {
            var entity = await Set.FindAsync(cancellationToken, keyValues);
            if (entity == null)
                return false;

            entity.TrackingState = TrackingState.Deleted;
            Context.ApplyChanges(entity);
            return true;
        }

        public async Task<TEntity> FindAsync(CancellationToken cancellationToken, params long[] keyValues)
        {
            return await Set.FindAsync(cancellationToken, keyValues);
        }

        public async Task<List<TEntity>> GetAllAsync(CancellationToken cancellationToken)
        {
            return await Set.ToListAsync(cancellationToken);
        }

        public async Task LoadRelatedEntitiesAsync(TEntity entity, CancellationToken cancellationToken)
        {
            await Context.LoadRelatedEntitiesAsync(entity, cancellationToken);
        }

        public async Task LoadRelatedEntitiesAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken)
        {
            await Context.LoadRelatedEntitiesAsync(entities, cancellationToken);
        }

        public void Insert(TEntity entity)
        {
            entity.TrackingState = TrackingState.Added;
            Context.ApplyChanges(entity);
        }

        public void Insert(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Insert(entity);
            }
        }

        public void Update(TEntity entity)
        {
            entity.TrackingState = TrackingState.Modified;
            this.Context.ApplyChanges(entity);

            var entityBase = entity as EntityBaseDal;
            if (entityBase != null)
            {
                var createdBy = this.Context.Entry(entityBase).Property(x => x.CreatedBy);
                createdBy.IsModified = false;

                var createdWhen = this.Context.Entry(entityBase).Property(x => x.CreatedWhen);
                createdWhen.IsModified = false;
            }
        }

        public void Update(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Update(entity);
            }
        }

        public async Task<List<TEntity>> SelectAsync(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, List<Expression<Func<TEntity, object>>> includes = null, int? page = null,
            int? pageSize = null, CancellationToken? cancellationToken = null)
        {
            return await this.Select(filter, orderBy, includes, page, pageSize).ToListAsync(cancellationToken ?? CancellationToken.None).ConfigureAwait(false);
        }

        internal IQueryable<TEntity> Select(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, List<Expression<Func<TEntity, object>>> includes = null, int? page = null, int? pageSize = null)
        {
            IQueryable<TEntity> query = this.Set;

            if (includes != null) query = includes.Aggregate(query, (current, include) => current.Include(include));

            if (orderBy != null) query = orderBy(query);

            if (filter != null) query = query.AsExpandable().Where(filter); 

            if (page != null && pageSize != null) query = query.Skip(page.Value * pageSize.Value).Take(pageSize.Value);

            // this.logger.WriteVerbose($"SQL: {query}");
            return query;
        }
    }
}
