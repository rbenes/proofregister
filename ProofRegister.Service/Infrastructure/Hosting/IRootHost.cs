﻿namespace ProofRegister.Service.Infrastructure.Hosting
{
    public interface IRootHost
    {
        /// <summary>
        ///     Nastartuje hostování.
        /// </summary>
        void Start();

        /// <summary>
        ///     Ukončí hostování.
        /// </summary>
        void Stop();
    }
}
