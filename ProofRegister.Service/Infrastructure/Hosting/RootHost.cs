﻿using System;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Service.Infrastructure.Communication;

namespace ProofRegister.Service.Infrastructure.Hosting
{
    public class RootHost : IRootHost
    {
        private readonly IWebApiModule webApiModule;
        private readonly ILogger logger;

        public RootHost(ILogger logger, IWebApiModule webApiModule)
        {
            this.webApiModule = webApiModule ?? throw new ArgumentNullException(nameof(webApiModule));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void Start()
        {
            logger.Write("Application starting.");
            webApiModule.StartAsync().Wait();
            logger.Write("Application started.");
        }

        public void Stop()
        {
            logger.Write("Application ending.");
            webApiModule.StopAsync().Wait();
            logger.Write("Application stoped.");
        }
    }
}
