﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProofRegister.Service.Infrastructure.Communication
{
    public interface IWebApiModule
    {
        Task StartAsync();

        Task StopAsync();
    }
}
