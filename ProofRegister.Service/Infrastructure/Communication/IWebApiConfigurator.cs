﻿using System.Web.Http.SelfHost;

namespace ProofRegister.Service.Infrastructure.Communication
{
    public interface IWebApiConfigurator
    {
        HttpSelfHostConfiguration GetConfiguration();
    }
}
