﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Contracts.Entities;
using ProofRegister.Contracts.Entities.Responses;
using ProofRegister.Service.Business.Commands.Controller;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Business.Queries.Controller;
using ProofRegister.Service.Infrastructure.WebApi;

namespace ProofRegister.Service.WebApi.Controllers
{
    public abstract class ApiControllerBase : ApiController
    {
        protected abstract string RouteName { get; }
    }

    public abstract class ApiControllerBase<TDtoEntity, TBusiness> : ApiControllerBase, IApiControllerBase<TDtoEntity>
        where TDtoEntity : EntityBaseDto
        where TBusiness : BusinessEntityBase
    {
        private readonly ICommandProcessor commandProcessor;
        private readonly IMapper mapper;
        private readonly IExceptionHandling handling;
        private readonly IQueryProcessor queryProcessor;

        protected ApiControllerBase(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor, IMapper mapper, IExceptionHandling handling)
        {
            this.commandProcessor = commandProcessor;
            this.queryProcessor = queryProcessor;
            this.mapper = mapper;
            this.handling = handling;
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> Create(TDtoEntity entity)
        {
            var mappedEntity = mapper.Map<TDtoEntity, TBusiness>(entity);
            var username = "TODO"; // TODO Use Request Metadata header
            var command = new ControllerCreateCommand<TBusiness>(mappedEntity, username);
            await commandProcessor.ProcessAsync(command, CancellationToken.None);
            return Ok();
        }

        [HttpDelete]
        public virtual async Task<IHttpActionResult> Delete(long id)
        {
            var command = new ControllerDeleteCommand<TBusiness>(id);
            await commandProcessor.ProcessAsync(command, CancellationToken.None);
            return Ok();
        }

        [HttpGet]
        public virtual async Task<IHttpActionResult> Get(int? page = null, int? pageSize = null)
        {
            try
            {
                var dataQuery = new ControllerGetEntitiesQuery<ICollection<TBusiness>>(page, pageSize);
                var data = await queryProcessor.ProcessAsync(dataQuery, CancellationToken.None);

                if (data == null)
                    return NotFound();

                var countQuery = new ControllerCountQuery<TBusiness>(typeof(TBusiness));
                var count = await queryProcessor.ProcessAsync(countQuery, CancellationToken.None);

                var response = new PagedEntitiesResponse<TDtoEntity>
                {
                    Entities = mapper.Map<IEnumerable<TBusiness>, IEnumerable<TDtoEntity>>(data),
                    ResultSetLimit = new ResultSetLimit
                    {
                        Count = count,
                        Page = page,
                        PageSize = pageSize
                    }
                };

                return Ok(response);
            }
            catch (Exception ex)
            {
                handling.HandleException(ex);
                return InternalServerError();
            }
        }

        [HttpGet]
        public virtual async Task<IHttpActionResult> Get(long id)
        {
            var query = new ControllerGetEntityQuery<TBusiness>(id);
            var response = await queryProcessor.ProcessAsync(query, CancellationToken.None);
            return response == null ? (IHttpActionResult) NotFound() : Ok(response);
        }

        [HttpPut]
        public virtual async Task<IHttpActionResult> Update(TDtoEntity entity)
        {
            var mappedEntity = mapper.Map<TDtoEntity, TBusiness>(entity);
            var username = "TODO"; // TODO Use Request Metadata header
            var command = new ControllerUpdateCommand<TBusiness>(mappedEntity, username);
            await commandProcessor.ProcessAsync(command, CancellationToken.None);
            return Ok();
        }
    }
}
