﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Contracts;
using ProofRegister.Contracts.Entities;
using ProofRegister.Service.Business.Entities;

namespace ProofRegister.Service.WebApi.Controllers
{
    public class TrackSegmentsController : ApiControllerBase<TrackSegmentDto, TrackSegment>
    {
        public TrackSegmentsController(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor, IMapper mapper, IExceptionHandling handling) : base(commandProcessor, queryProcessor, mapper, handling)
        {
        }

        public override Task<IHttpActionResult> Get(int? page = null, int? pageSize = null)
        {
            return Task.FromResult<IHttpActionResult>(BadRequest());
        }

        [HttpGet]
        [Route("track-segments/for-track/{id}")]
        public Task<IHttpActionResult> ForTrack(int id)
        {
            // TODO
            var data = new List<int>{1,5,7};
            return Task.FromResult<IHttpActionResult>(Ok(data));
        }

        protected override string RouteName => RouteNames.TrackSegments;
    }
}
