﻿using AutoMapper;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Contracts.Entities;
using ProofRegister.Service.Business.Entities;

namespace ProofRegister.Service.WebApi.Controllers
{
    public class TrackingDevicesController : ApiControllerBase<TrackingDeviceDto, TrackingDevice>
    {
        public TrackingDevicesController(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor, IMapper mapper, IExceptionHandling handling)
            : base(commandProcessor, queryProcessor, mapper, handling)
        {
        }

        protected override string RouteName => RouteNames.TrackingDevices;
    }
}
