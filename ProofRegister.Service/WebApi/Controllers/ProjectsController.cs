﻿using AutoMapper;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Contracts.Entities;
using ProofRegister.Service.Business.Entities;

namespace ProofRegister.Service.WebApi.Controllers
{
    public class ProjectsController : ApiControllerBase<ProjectDto, Project>
    {
        public ProjectsController(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor, IMapper mapper, IExceptionHandling handling)
            : base(commandProcessor, queryProcessor, mapper, handling)
        {
        }

        protected override string RouteName => RouteNames.Projects;
    }
}
