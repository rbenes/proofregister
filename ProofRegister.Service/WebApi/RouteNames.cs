﻿namespace ProofRegister.Service.WebApi
{
    public class RouteNames
    {
        public const string Projects = "Projects";
        public const string Tracks = "Tracks";
        public const string TrackSegments = "TrackSegments";
        public const string TrackingDevices = "TrackingDevices";
    }
}
