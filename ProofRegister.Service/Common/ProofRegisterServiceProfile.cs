﻿using AutoMapper;
using ProofRegister.Contracts.Entities;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;

namespace ProofRegister.Service.Common
{
    public class ProofRegisterServiceProfile : Profile
    {
        public ProofRegisterServiceProfile()
        {
            CreateMap<Address, AddressDal>();
            CreateMap<Address, AddressDto>();
            CreateMap<AddressDal, Address>();
            CreateMap<AddressDto, Address>();

            CreateMap<NielsenShopProjectPoint, NielsenShopProjectPointDal>();
            CreateMap<NielsenShopProjectPoint, NielsenShopProjectPointDto>();
            CreateMap<NielsenShopProjectPointDal, NielsenShopProjectPoint>();
            CreateMap<NielsenShopProjectPointDto, NielsenShopProjectPoint>();

            CreateMap<Position, PositionDal>();
            CreateMap<Position, PositionDto>();
            CreateMap<PositionDal, Position>();
            CreateMap<PositionDto, Position>();

            CreateMap<Project, ProjectDal>();
            CreateMap<Project, ProjectDto>();
            CreateMap<ProjectDal, Project>();
            CreateMap<ProjectDto, Project>();

            CreateMap<ProjectPeriod, ProjectPeriodDal>();
            CreateMap<ProjectPeriod, ProjectPeriodDto>();
            CreateMap<ProjectPeriodDal, ProjectPeriod>();
            CreateMap<ProjectPeriodDto, ProjectPeriod>();

            CreateMap<ProjectPoint, ProjectPointDal>();
            CreateMap<ProjectPoint, ProjectPointDto>();
            CreateMap<ProjectPointDal, ProjectPoint>();
            CreateMap<ProjectPointDto, ProjectPoint>();

            CreateMap<ProjectPointType, ProjectPointTypeDal>();
            CreateMap<ProjectPointType, ProjectPointTypeDto>();
            CreateMap<ProjectPointTypeDal, ProjectPointType>();
            CreateMap<ProjectPointTypeDto, ProjectPointType>();

            CreateMap<ProjectPointVisit, ProjectPointVisitDal>();
            CreateMap<ProjectPointVisit, ProjectPointVisitDto>();
            CreateMap<ProjectPointVisitDal, ProjectPointVisit>();
            CreateMap<ProjectPointVisitDto, ProjectPointVisit>();

            CreateMap<Track, TrackDal>();
            CreateMap<Track, TrackDto>();
            CreateMap<TrackDal, Track>();
            CreateMap<TrackDto, Track>();

            CreateMap<TrackingDevice, TrackingDeviceDal>();
            CreateMap<TrackingDevice, TrackingDeviceDto>();
            CreateMap<TrackingDeviceDal, TrackingDevice>();
            CreateMap<TrackingDeviceDto, TrackingDevice>();

            CreateMap<TrackSegment, TrackSegmentDal>();
            CreateMap<TrackSegment, TrackSegmentDto>();
            CreateMap<TrackSegmentDal, TrackSegment>();
            CreateMap<TrackSegmentDto, TrackSegment>();

            CreateMap<TrackStatus, TrackStatusDal>();
            CreateMap<TrackStatus, TrackStatusDto>();
            CreateMap<TrackStatusDal, TrackStatus>();
            CreateMap<TrackStatusDto, TrackStatus>();

            CreateMap<VisitInfo, VisitInfoDal>();
            CreateMap<VisitInfo, VisitInfoDto>();
            CreateMap<VisitInfoDal, VisitInfo>();
            CreateMap<VisitInfoDto, VisitInfo>();
        }
    }
}
