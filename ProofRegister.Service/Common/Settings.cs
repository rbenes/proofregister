﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProofRegister.Service.Common
{
    public class Settings : ISettings
    {
        public string BaseUrl { get; set; }

        public Settings()
        {
            BaseUrl = "http://localhost:1001/ProofRegister";
        }
    }
}
