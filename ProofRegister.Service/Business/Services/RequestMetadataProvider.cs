﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using ProofRegister.Common;
using ProofRegister.Service.Infrastructure.Services;

namespace ProofRegister.Service.Business.Services
{
    public class RequestMetadataProvider : IRequestMetadataProvider
    {
        public string GetUsername(HttpRequestMessage request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var username = this.GetHeaderValue(request, Constants.HttpUsernameHeader);
            if (String.IsNullOrWhiteSpace(username))
                throw new NullReferenceException(nameof(username));

            return username;
        }

        private string GetHeaderValue(HttpRequestMessage request, string key)
        {
            IEnumerable<string> values;
            request.Headers.TryGetValues(key, out values);
            return values?.SingleOrDefault();
        }
    }
}
