﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Infrastructure.Factories;
using ProofRegister.Service.Infrastructure.Services;

namespace ProofRegister.Service.Business.Services
{
    public class DataService<TDalEntity> : IDataService<TDalEntity> where TDalEntity : EntityBaseDal
    {
        private readonly IUnitOfWorkFactory unitOfWorkFactory;

        public DataService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory ?? throw new ArgumentNullException(nameof(unitOfWorkFactory));
        }

        public async Task<int> CountAsync(Expression<Func<TDalEntity, bool>> filter, CancellationToken cancellationToken)
        {
            using (var uow = unitOfWorkFactory.GetUnitOfWork())
            {
                var repo = uow.GetRepository<TDalEntity>();
                return await repo.CountAsync(filter, cancellationToken);
            }
        }

        public async Task<TDalEntity> CreateAsync(TDalEntity entity, CancellationToken cancellationToken, string username)
        {
            using (var uow = unitOfWorkFactory.GetUnitOfWork())
            {
                var repo = uow.GetRepository<TDalEntity>();
                entity.CreatedWhen = DateTime.Now;
                entity.CreatedBy = username;
                repo.Insert(entity);
                await uow.SaveChangesAsync(cancellationToken);
            }

            return entity;
        }

        public async Task<bool> DeleteAsync(long id, CancellationToken cancellationToken)
        {
            using (var uow = unitOfWorkFactory.GetUnitOfWork())
            {
                var repo = uow.GetRepository<TDalEntity>();
                var deleted = await repo.DeleteAsync(cancellationToken, id);
                await uow.SaveChangesAsync(cancellationToken);
                return deleted;
            }
        }

        public async Task DeleteAsync(TDalEntity entity, CancellationToken cancellationToken)
        {
            if (entity?.Id != null)
                await DeleteAsync(entity.Id.Value, cancellationToken);
        }

        public async Task<IEnumerable<TDalEntity>> GetsPagedAsync(Expression<Func<TDalEntity, bool>> filter, Func<IQueryable<TDalEntity>, IOrderedQueryable<TDalEntity>> orderBy, IEnumerable<Expression<Func<TDalEntity, object>>> includes, int? page, int? pageSize,
            CancellationToken cancellationToken)
        {
            using (var uow = unitOfWorkFactory.GetUnitOfWork())
            {
                var repo = uow.GetRepository<TDalEntity>();
                return await repo.SelectAsync(filter, orderBy, includes?.ToList(), page, pageSize, cancellationToken);
            }
        }

        public async Task<TDalEntity> UpdateAsync(TDalEntity entity, CancellationToken cancellationToken, string username)
        {
            using (var uow = unitOfWorkFactory.GetUnitOfWork())
            {
                var repo = uow.GetRepository<TDalEntity>();
                entity.ModifiedWhen = DateTime.Now;
                entity.ModifiedBy = username;
                repo.Update(entity);
                await uow.SaveChangesAsync(cancellationToken);
            }

            return entity;
        }

        public async Task<IEnumerable<TDalEntity>> UpdateAsync(IEnumerable<TDalEntity> entities, CancellationToken cancellationToken, string username)
        {
            var dalEntities = entities as IList<TDalEntity> ?? entities.ToList();

            using (var uow = unitOfWorkFactory.GetUnitOfWork())
            {
                var repo = uow.GetRepository<TDalEntity>();
                foreach (var dalEntity in dalEntities)
                {
                    dalEntity.ModifiedWhen = DateTime.Now;
                    dalEntity.ModifiedBy = username;
                }
                repo.Update(dalEntities);
                await uow.SaveChangesAsync(cancellationToken);
            }

            return dalEntities;
        }
    }
}
