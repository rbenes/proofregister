﻿using System;

namespace ProofRegister.Service.Business.Entities
{
    public class ProjectPeriod : BusinessEntityBase
    {
        public long ProjectId { get; set; }

        public Project Project { get; set; }

        public string Name { get; set; }

        public DateTime? ValidFrom { get; set; }

        public DateTime? ValidTo { get; set; }
    }
}
