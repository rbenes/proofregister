﻿namespace ProofRegister.Service.Business.Entities
{
    public abstract class ProjectPoint : BusinessEntityBase
    {
        protected ProjectPoint()
        {
            Position = new Position();
            Address = new Address();
        }

        public long ProjectId { get; set; }

        public Project Project { get; set; }

        public string Name { get; set; }

        public Position Position { get; set; }

        public decimal Radius { get; set; }

        public ProjectPointType ProjectPointType { get; set; }

        public long ProjectPointTypeId { get; set; }

        public Address Address { get; set; }
    }
}
