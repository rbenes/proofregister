﻿using System;
using System.Collections.Generic;

namespace ProofRegister.Service.Business.Entities
{
    public class Track : BusinessEntityBase
    {
        public DateTime ImportTime { get; set; }

        public TrackStatus TrackStatus { get; set; }

        public long TrackStatusId { get; set; }

        public decimal Distance { get; set; }

        public ICollection<TrackSegment> Segments { get; set; }

        public Track()
        {
            Segments = new List<TrackSegment>();
        }
    }
}
