﻿namespace ProofRegister.Service.Business.Entities
{
    public class Position
    {
        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }
    }
}
