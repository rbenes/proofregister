﻿using System;

namespace ProofRegister.Service.Business.Entities
{
    public class TrackSegment : BusinessEntityBase
    {
        public long TrackId { get; set; }

        public Track Track { get; set; }

        public Position Position { get; set; }

        public decimal? Elevation { get; set; }

        public DateTime SegmentTime { get; set; }

        public TrackSegment()
        {
            Position = new Position();
        }
    }
}
