﻿using System;
using System.Collections.Generic;

namespace ProofRegister.Service.Business.Entities
{
    public class Project : BusinessEntityBase
    {
        public string Name { get; set; }

        public DateTime? ValidFrom { get; set; }

        public DateTime? ValidTo { get; set; }

        public ICollection<ProjectPeriod> Periods { get; set; }

        public ICollection<ProjectPoint> Points { get; set; }

        public ICollection<TrackingDevice> Devices { get; set; }

        public Project()
        {
            Periods = new List<ProjectPeriod>();
            Points = new List<ProjectPoint>();
            Devices = new List<TrackingDevice>();
        }
    }
}
