﻿namespace ProofRegister.Service.Business.Entities
{
    public class TrackingDevice : BusinessEntityBase
    {
        public string KudyId { get; set; }

        public bool Enabled { get; set; }
    }
}
