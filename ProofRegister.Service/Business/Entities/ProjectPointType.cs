﻿namespace ProofRegister.Service.Business.Entities
{
    public class ProjectPointType : BusinessEntityBase
    {
        public long ProjectId { get; set; }

        public Project Project { get; set; }

        public string Name { get; set; }
    }
}
