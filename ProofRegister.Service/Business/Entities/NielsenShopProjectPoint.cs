﻿namespace ProofRegister.Service.Business.Entities
{
    public class NielsenShopProjectPoint : ProjectPoint
    {
        public string Ico { get; set; }

        public string Dic { get; set; }

        public string IdNielsen { get; set; }

        public string StoreCode { get; set; }
    }
}
