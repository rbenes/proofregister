﻿using System;

namespace ProofRegister.Service.Business.Entities
{
    public abstract class BusinessEntityBase
    {
        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public long? Id { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public byte[] RowVersion { get; set; }

        public override string ToString()
        {
            return $"{GetType().Name}";
        }
    }
}
