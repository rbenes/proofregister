﻿namespace ProofRegister.Service.Business.Entities
{
    public class ProjectPointVisit : BusinessEntityBase
    {
        public long ProjectPointId { get; set; }

        public ProjectPoint ProjectPoint { get; set; }

        public long ProjectPeriodId { get; set; }

        public ProjectPeriod ProjectPeriod { get; set; }

        public long TrackId { get; set; }

        public Track Track { get; set; }

        public VisitInfo RealTrackInfo { get; set; }

        public VisitInfo ProtocolInfo { get; set; }

        public string ProjectPointInfo { get; set; }

        public ProjectPointVisit()
        {
            RealTrackInfo = new VisitInfo();
            ProtocolInfo = new VisitInfo();
        }
    }
}
