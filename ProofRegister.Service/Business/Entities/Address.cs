﻿namespace ProofRegister.Service.Business.Entities
{
    public class Address
    {
        public string Street { get; set; }

        public string City { get; set; }

        public string PostCode { get; set; }

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(Street)}: {Street}, {nameof(City)}: {City}, {nameof(PostCode)}: {PostCode}";
        }
    }
}
