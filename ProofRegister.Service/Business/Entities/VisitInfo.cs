﻿using System;

namespace ProofRegister.Service.Business.Entities
{
    public class VisitInfo
    {
        public VisitInfo()
        {
            EntryPosition = new Position();
            ExitPosition = new Position();
        }

        public DateTime EntryTime { get; set; }

        public Position EntryPosition { get; set; }

        public DateTime ExitTime { get; set; }

        public Position ExitPosition { get; set; }
    }
}
