﻿namespace ProofRegister.Service.Business.Entities
{
    public class TrackStatus : BusinessEntityBase
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
