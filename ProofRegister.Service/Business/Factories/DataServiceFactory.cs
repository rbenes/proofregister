﻿using System;
using ProofRegister.Common.Infrastructure;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Services;
using ProofRegister.Service.Infrastructure.Factories;
using ProofRegister.Service.Infrastructure.Services;

namespace ProofRegister.Service.Business.Factories
{
    public class DataServiceFactory : IDataServiceFactory
    {
        private readonly ILogger logger;
        private readonly IUnitOfWorkFactory uowFactory;

        public DataServiceFactory(IUnitOfWorkFactory uowFactory,
            ILogger logger)
        {
            this.uowFactory = uowFactory ?? throw new ArgumentNullException(nameof(uowFactory));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public IDataService<TDalEntity> GetDataService<TDalEntity>()
            where TDalEntity : EntityBaseDal
        {
            return new DataService<TDalEntity>(uowFactory);
        }
    }
}
