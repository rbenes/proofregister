﻿using ProofRegister.DAL;
using ProofRegister.Service.Infrastructure.DAL;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Factories
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        public IUnitOfWork GetUnitOfWork()
        {
            return new ProofRegisterUnitOfWork(new ProofRegisterDbContext());
        }
    }
}
