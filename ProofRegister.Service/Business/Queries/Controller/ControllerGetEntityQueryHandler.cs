﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;
using ProofRegister.Service.Infrastructure.Services;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class ControllerGetEntityQueryHandler<TBusiness, TDalEntity> : IQueryHandler<ControllerGetEntityQuery<TBusiness>, TBusiness>
        where TBusiness : BusinessEntityBase
        where TDalEntity : EntityBaseDal
    {
        private readonly IDataService<TDalEntity> dataService;
        private readonly IMapper mapper;

        public ControllerGetEntityQueryHandler(IDataServiceFactory dataServiceFactory, IMapper mapper)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            dataService = dataServiceFactory.GetDataService<TDalEntity>() ?? throw new ArgumentNullException(nameof(dataServiceFactory));
        }

        public async Task<TBusiness> HandleAsync(ControllerGetEntityQuery<TBusiness> query, CancellationToken cancellationToken)
        {
            var data = await dataService.GetsPagedAsync(f => f.Id == query.Id, null, null, null, null, cancellationToken);
            var dalEntity = data?.SingleOrDefault();
            return mapper.Map<TDalEntity, TBusiness>(dalEntity);
        }
    }
}
