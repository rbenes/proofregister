﻿using AutoMapper;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class TrackingDeviceGetEntityQueryHandler : ControllerGetEntityQueryHandler<TrackingDevice, TrackingDeviceDal>
    {
        public TrackingDeviceGetEntityQueryHandler(IDataServiceFactory dataServiceFactory, IMapper mapper)
            : base(dataServiceFactory, mapper)
        {
        }
    }
}
