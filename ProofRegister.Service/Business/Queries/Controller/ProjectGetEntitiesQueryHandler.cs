﻿using AutoMapper;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class ProjectGetEntitiesQueryHandler : ControllerGetEntitiesQueryHandler<Project, ProjectDal>
    {
        public ProjectGetEntitiesQueryHandler(IDataServiceFactory dataServiceFactory, IMapper mapper)
            : base(dataServiceFactory, mapper)
        {
        }
    }
}
