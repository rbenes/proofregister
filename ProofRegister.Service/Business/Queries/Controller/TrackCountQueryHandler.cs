﻿using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class TrackCountQueryHandler : ControllerCountQueryHandler<Track, TrackDal>
    {
        public TrackCountQueryHandler(IDataServiceFactory dataServiceFactory)
            : base(dataServiceFactory)
        {
        }
    }
}
