﻿using AutoMapper;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class TrackingDeviceGetEntitiesQueryHandler : ControllerGetEntitiesQueryHandler<TrackingDevice, TrackingDeviceDal>
    {
        public TrackingDeviceGetEntitiesQueryHandler(IDataServiceFactory dataServiceFactory, IMapper mapper)
            : base(dataServiceFactory, mapper)
        {
        }
    }
}
