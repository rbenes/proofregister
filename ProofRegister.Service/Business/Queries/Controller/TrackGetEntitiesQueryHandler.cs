﻿using AutoMapper;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class TrackGetEntitiesQueryHandler : ControllerGetEntitiesQueryHandler<Track, TrackDal>
    {
        public TrackGetEntitiesQueryHandler(IDataServiceFactory dataServiceFactory, IMapper mapper)
            : base(dataServiceFactory, mapper)
        {
        }
    }
}
