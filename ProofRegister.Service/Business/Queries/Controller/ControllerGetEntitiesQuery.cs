﻿using ProofRegister.Common.Infrastructure.Queries;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class ControllerGetEntitiesQuery<TBusiness> : QueryBase<TBusiness>
    {
        public int? Page { get; }
        public int? PageSize { get; }

        public ControllerGetEntitiesQuery(int? page, int? pageSize)
        {
            Page = page;
            PageSize = pageSize;
        }
    }
}
