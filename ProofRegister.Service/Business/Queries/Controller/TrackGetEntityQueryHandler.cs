﻿using AutoMapper;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class TrackGetEntityQueryHandler : ControllerGetEntityQueryHandler<Track, TrackDal>
    {
        public TrackGetEntityQueryHandler(IDataServiceFactory dataServiceFactory, IMapper mapper)
            : base(dataServiceFactory, mapper)
        {
        }
    }
}
