﻿using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class ProjectCountQueryHandler : ControllerCountQueryHandler<Project, ProjectDal>
    {
        public ProjectCountQueryHandler(IDataServiceFactory dataServiceFactory)
            : base(dataServiceFactory)
        {
        }
    }
}
