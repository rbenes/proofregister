﻿using System;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Service.Business.Entities;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class ControllerCountQuery<TBusiness> : QueryBase<int>
        where TBusiness : BusinessEntityBase
    {
        public Type EntityType { get; }

        public ControllerCountQuery(Type entityType)
        {
            if (!typeof(TBusiness).IsAssignableFrom(entityType))
                throw new NotSupportedException();

            EntityType = entityType;
        }
    }
}
