﻿using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class TrackingDeviceCountQueryHandler : ControllerCountQueryHandler<TrackingDevice, TrackingDeviceDal>
    {
        public TrackingDeviceCountQueryHandler(IDataServiceFactory dataServiceFactory)
            : base(dataServiceFactory)
        {
        }
    }
}
