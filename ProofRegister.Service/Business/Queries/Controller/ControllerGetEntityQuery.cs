﻿using ProofRegister.Common.Infrastructure.Queries;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class ControllerGetEntityQuery<TBusiness> : QueryBase<TBusiness>
    {
        public ControllerGetEntityQuery(long id)
        {
            Id = id;
        }

        public long Id { get; }
    }
}
