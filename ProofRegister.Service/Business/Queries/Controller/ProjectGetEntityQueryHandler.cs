﻿using AutoMapper;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class ProjectGetEntityQueryHandler : ControllerGetEntityQueryHandler<Project, ProjectDal>
    {
        public ProjectGetEntityQueryHandler(IDataServiceFactory dataServiceFactory, IMapper mapper)
            : base(dataServiceFactory, mapper)
        {
        }
    }
}
