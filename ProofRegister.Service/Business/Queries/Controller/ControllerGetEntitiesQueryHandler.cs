﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;
using ProofRegister.Service.Infrastructure.Services;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class ControllerGetEntitiesQueryHandler<TBusiness, TDalEntity> : IQueryHandler<ControllerGetEntitiesQuery<ICollection<TBusiness>>, ICollection<TBusiness>>
        where TBusiness : BusinessEntityBase
        where TDalEntity : EntityBaseDal
    {
        private readonly IMapper mapper;
        private readonly IDataService<TDalEntity> dataService;

        public ControllerGetEntitiesQueryHandler(IDataServiceFactory dataServiceFactory, IMapper mapper)
        {
            this.mapper = mapper;
            this.dataService = dataServiceFactory.GetDataService<TDalEntity>() ?? throw new ArgumentNullException(nameof(dataServiceFactory));
        }

        public async Task<ICollection<TBusiness>> HandleAsync(ControllerGetEntitiesQuery<ICollection<TBusiness>> query, CancellationToken cancellationToken)
        {
            var entities = await dataService.GetsPagedAsync(null, null, null, null, null, cancellationToken);
            return mapper.Map<ICollection<TDalEntity>, ICollection<TBusiness>>(entities?.ToList());
        }
    }
}
