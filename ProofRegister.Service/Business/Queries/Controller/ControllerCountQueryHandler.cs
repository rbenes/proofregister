﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;
using ProofRegister.Service.Infrastructure.Services;

namespace ProofRegister.Service.Business.Queries.Controller
{
    public class ControllerCountQueryHandler<TBusiness, TDalEnity> : IQueryHandler<ControllerCountQuery<TBusiness>, int>
        where TDalEnity : EntityBaseDal
        where TBusiness : BusinessEntityBase
    {
        private readonly IDataService<TDalEnity> dataService;

        public ControllerCountQueryHandler(IDataServiceFactory dataServiceFactory)
        {
            this.dataService = dataServiceFactory.GetDataService<TDalEnity>() ?? throw new ArgumentNullException(nameof(dataServiceFactory));
        }

        public async Task<int> HandleAsync(ControllerCountQuery<TBusiness> query, CancellationToken cancellationToken)
        {
            if (query.EntityType != typeof(TBusiness))
                throw new NotSupportedException(query.EntityType.Name);
            
            return await dataService.CountAsync(null, cancellationToken);
        }
    }
}
