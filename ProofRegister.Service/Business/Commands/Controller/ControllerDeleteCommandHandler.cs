﻿using System.Threading;
using System.Threading.Tasks;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;
using ProofRegister.Service.Infrastructure.Services;

namespace ProofRegister.Service.Business.Commands.Controller
{
    public class ControllerDeleteCommandHandler<TBusiness, TDalEntity> : ICommandHandler<ControllerDeleteCommand<TBusiness>>
        where TBusiness : BusinessEntityBase
        where TDalEntity : EntityBaseDal
    {
        private readonly IDataService<TDalEntity> dataService;

        public ControllerDeleteCommandHandler(IDataServiceFactory dataService)
        {
            this.dataService = dataService.GetDataService<TDalEntity>();
        }

        public virtual async Task HandleAsync(ControllerDeleteCommand<TBusiness> command, CancellationToken cancellationToken)
        {
            await dataService.DeleteAsync(command.Id, cancellationToken);
        }
    }
}
