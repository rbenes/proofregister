﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;
using ProofRegister.Service.Infrastructure.Services;

namespace ProofRegister.Service.Business.Commands.Controller
{
    public class ControllerCreateCommandHandler<TBusiness, TDalEntity> : ICommandHandler<ControllerCreateCommand<TBusiness>>
        where TBusiness : BusinessEntityBase
        where TDalEntity : EntityBaseDal
    {
        private readonly IDataService<TDalEntity> dataService;
        private readonly IMapper mapper;

        public ControllerCreateCommandHandler(IMapper mapper, IDataServiceFactory dataService)
        {
            this.mapper = mapper;
            this.dataService = dataService.GetDataService<TDalEntity>();
        }

        public virtual async Task HandleAsync(ControllerCreateCommand<TBusiness> command, CancellationToken cancellationToken)
        {
            var dalEntity = mapper.Map<TBusiness, TDalEntity>(command.Entity);
            await dataService.CreateAsync(dalEntity, cancellationToken, command.Username);
        }
    }
}
