﻿using AutoMapper;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Commands.Controller
{
    public class TrackUpdateCommandHandler : ControllerUpdateCommandHandler<Track, TrackDal>
    {
        public TrackUpdateCommandHandler(IMapper mapper, IDataServiceFactory dataService)
            : base(mapper, dataService)
        {
        }
    }
}
