﻿using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Service.Business.Entities;

namespace ProofRegister.Service.Business.Commands.Controller
{
    public class ControllerUpdateCommand<TBusiness> : CommandBase
        where TBusiness : BusinessEntityBase
    {
        public ControllerUpdateCommand(TBusiness entity, string username)
        {
            Entity = entity;
            Username = username;
        }

        public TBusiness Entity { get; }
        public string Username { get; set; }
    }
}
