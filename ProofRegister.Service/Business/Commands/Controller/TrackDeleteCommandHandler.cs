﻿using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Commands.Controller
{
    public class TrackDeleteCommandHandler : ControllerDeleteCommandHandler<Track, TrackDal>
    {
        public TrackDeleteCommandHandler(IDataServiceFactory dataService)
            : base(dataService)
        {
        }
    }
}
