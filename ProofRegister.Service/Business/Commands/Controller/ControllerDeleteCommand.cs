﻿using System;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Service.Business.Entities;

namespace ProofRegister.Service.Business.Commands.Controller
{
    public class ControllerDeleteCommand<TBusiness> : CommandBase
        where TBusiness : BusinessEntityBase
    {
        public ControllerDeleteCommand(TBusiness entity)
        {
            Id = entity?.Id ?? throw new ArgumentNullException(nameof(entity));
        }

        public ControllerDeleteCommand(long id)
        {
            Id = id;
        }

        public long Id { get; set; }
    }
}
