﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;
using ProofRegister.Service.Infrastructure.Services;

namespace ProofRegister.Service.Business.Commands.Controller
{
    public class ControllerUpdateCommandHandler<TBusiness, TDalEntity> : ICommandHandler<ControllerUpdateCommand<TBusiness>>
        where TBusiness : BusinessEntityBase
        where TDalEntity : EntityBaseDal
    {
        private readonly IDataService<TDalEntity> dataService;
        private readonly IMapper mapper;

        public ControllerUpdateCommandHandler(IMapper mapper, IDataServiceFactory dataService)
        {
            this.mapper = mapper;
            this.dataService = dataService.GetDataService<TDalEntity>();
        }

        public virtual async Task HandleAsync(ControllerUpdateCommand<TBusiness> command, CancellationToken cancellationToken)
        {
            var dalEntity = mapper.Map<TBusiness, TDalEntity>(command.Entity);
            await dataService.UpdateAsync(dalEntity, cancellationToken, command.Username);
        }
    }
}
