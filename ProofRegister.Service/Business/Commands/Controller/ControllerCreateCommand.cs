﻿using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Service.Business.Entities;

namespace ProofRegister.Service.Business.Commands.Controller
{
    public class ControllerCreateCommand<TBusiness> : CommandBase
        where TBusiness : BusinessEntityBase
    {
        public ControllerCreateCommand(TBusiness mappedEntity, string username)
        {
            Entity = mappedEntity;
            Username = username;
        }

        public TBusiness Entity { get; }

        public string Username { get; }
    }
}
