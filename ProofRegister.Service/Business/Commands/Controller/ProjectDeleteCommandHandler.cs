﻿using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Commands.Controller
{
    public class ProjectDeleteCommandHandler : ControllerDeleteCommandHandler<Project, ProjectDal>
    {
        public ProjectDeleteCommandHandler(IDataServiceFactory dataService)
            : base(dataService)
        {
        }
    }
}
