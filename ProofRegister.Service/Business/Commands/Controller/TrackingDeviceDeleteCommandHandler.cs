﻿using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Commands.Controller
{
    public class TrackingDeviceDeleteCommandHandler : ControllerDeleteCommandHandler<TrackingDevice, TrackingDeviceDal>
    {
        public TrackingDeviceDeleteCommandHandler(IDataServiceFactory dataService)
            : base(dataService)
        {
        }
    }
}
