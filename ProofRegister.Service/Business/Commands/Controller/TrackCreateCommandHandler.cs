﻿using AutoMapper;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Commands.Controller
{
    public class TrackCreateCommandHandler : ControllerCreateCommandHandler<Track, TrackDal>
    {
        public TrackCreateCommandHandler(IMapper mapper, IDataServiceFactory dataService)
            : base(mapper, dataService)
        {
        }
    }
}
