﻿using AutoMapper;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Commands.Controller
{
    public class ProjectCreateCommandHandler : ControllerCreateCommandHandler<Project, ProjectDal>
    {
        public ProjectCreateCommandHandler(IMapper mapper, IDataServiceFactory dataService)
            : base(mapper, dataService)
        {
        }
    }
}
