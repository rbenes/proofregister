﻿using AutoMapper;
using ProofRegister.DAL.Entities;
using ProofRegister.Service.Business.Entities;
using ProofRegister.Service.Infrastructure.Factories;

namespace ProofRegister.Service.Business.Commands.Controller
{
    public class TrackingDeviceUpdateCommandHandler : ControllerUpdateCommandHandler<TrackingDevice, TrackingDeviceDal>
    {
        public TrackingDeviceUpdateCommandHandler(IMapper mapper, IDataServiceFactory dataService)
            : base(mapper, dataService)
        {
        }
    }
}
