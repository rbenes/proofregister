﻿using System;
using System.Web.Http;
using System.Web.Http.Dependencies;
using System.Web.Http.SelfHost;
using ProofRegister.Contracts;
using ProofRegister.Contracts.Entities;
using ProofRegister.Service.Common;
using ProofRegister.Service.Infrastructure.Communication;
using ProofRegister.Service.WebApi;
using ProofRegister.Service.WebApi.Controllers;

namespace ProofRegister.Service.Business.Communication
{
    public class WebApiConfigurator : IWebApiConfigurator
    {
        private const string DefaultRoutePrefix = "api/v1/";
        private readonly IDependencyResolver dependencyResolver;
        private readonly ISettings settings;

        public WebApiConfigurator(ISettings settings, IDependencyResolver dependencyResolver)
        {
            this.dependencyResolver = dependencyResolver ?? throw new ArgumentNullException(nameof(dependencyResolver));
            this.settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        public HttpSelfHostConfiguration GetConfiguration()
        {
            // osetreni, aby BaseUrl koncilo prave jednim lomitkem bez ohledu na to, jestli v configu je nebo ne
            var baseUrl = settings.BaseUrl.TrimEnd('/') + "/";
            var config = new HttpSelfHostConfiguration($"{baseUrl}{DefaultRoutePrefix}");
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(RouteNames.Tracks, $"{WebApiCommon.GetServiceName<TrackDto>()}/{{id}}",
                new { controller = GeControllerName<TracksController>(), id = RouteParameter.Optional });

            config.Routes.MapHttpRoute(RouteNames.Projects, $"{WebApiCommon.GetServiceName<ProjectDto>()}/{{id}}",
                new { controller = GeControllerName<ProjectsController>(), id = RouteParameter.Optional });

            config.Routes.MapHttpRoute(RouteNames.TrackingDevices, $"{WebApiCommon.GetServiceName<TrackingDeviceDto>()}/{{id}}",
                new { controller = GeControllerName<TrackingDevicesController>(), id = RouteParameter.Optional });
            
            config.Routes.MapHttpRoute(RouteNames.TrackSegments, $"{WebApiCommon.GetServiceName<TrackSegmentDto>()}/{{id}}",
                new { controller = GeControllerName<TrackSegmentsController>(), id = RouteParameter.Optional });

            config.DependencyResolver = dependencyResolver;

            return config;
        }

        private static string GeControllerName<T>()
        {
            return typeof(T).Name.Replace("Controller", "");
        }
    }
}
