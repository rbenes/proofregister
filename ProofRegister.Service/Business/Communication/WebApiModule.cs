﻿using System;
using System.Threading.Tasks;
using System.Web.Http.SelfHost;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Service.Common;
using ProofRegister.Service.Infrastructure.Communication;

namespace ProofRegister.Service.Business.Communication
{
    public class WebApiModule : IWebApiModule
    {
        private readonly ISettings settings;
        private readonly ILogger logger;
        private HttpSelfHostServer server;

        private readonly IWebApiConfigurator configurator;

        public WebApiModule(ISettings settings, ILogger logger, IWebApiConfigurator webApiConfigurator)
        {
            this.settings = settings ?? throw new ArgumentNullException(nameof(settings));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            configurator = webApiConfigurator ?? throw new ArgumentNullException(nameof(webApiConfigurator));
        }

        public async Task StartAsync()
        {
            this.logger.WriteInfo($"Starting WebAPI self host on {this.settings.BaseUrl}...");

            var config = this.configurator.GetConfiguration();
            server = new HttpSelfHostServer(config);
            await this.server.OpenAsync().ConfigureAwait(false);
            logger.WriteInfo($"WebAPI self host started and listening on {this.settings.BaseUrl}");
        }

        public async Task StopAsync()
        {
            if (this.server != null)
                await this.server.CloseAsync().ConfigureAwait(false);
        }
    }
}
