﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ProofRegister.Common;
using ProofRegister.DAL.Entities;

namespace ProofRegister.DAL
{
    public class ProofRegisterDbContext : DbContext
    {
        public DbSet<ProjectDal> Projects { get; set; }
        public DbSet<ProjectPeriodDal> ProjectPeriods { get; set; }
        public DbSet<ProjectPointDal> ProjectPoints { get; set; }
        public DbSet<ProjectPointTypeDal> ProjectPointTypes { get; set; }
        public DbSet<ProjectPointVisitDal> ProjectPointVisits { get; set; }
        public DbSet<TrackDal> Tracks { get; set; }
        public DbSet<TrackStatusDal> TrackStatuses { get; set; }

        public ProofRegisterDbContext(bool checkDbModelCompatibility)
            : this(Constants.ConnectionStringName, checkDbModelCompatibility)
        {
        }

        // musi byt defaultni bezparametricky konstruktor kvuli EF migracim
        public ProofRegisterDbContext()
            : this(Constants.ConnectionStringName, false)
        {
        }

        public ProofRegisterDbContext(string nameOrConnectionString, bool checkDbModelCompatibility)
            : base(nameOrConnectionString)
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.ValidateOnSaveEnabled = true;
            
            Database.SetInitializer(new NullDatabaseInitializer<ProofRegisterDbContext>());

            if (checkDbModelCompatibility)
                CheckDbModelCompatibility();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(Constants.DatabaseSchema);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
        }

        private void CheckDbModelCompatibility()
        {
            bool isValid;

            try
            {
                isValid = Database.CompatibleWithModel(true);
            }
            catch (Exception ex)
            {
                //throw DbModelNotCompatibleException.CreateDbCompatibilyCheckFailed(ex);
                throw new Exception("DbCompatibilityProblem", ex);
            }

            if (!isValid)
                throw new Exception();
            //throw DbModelNotCompatibleException.CreateDbNotCompatible();
        }
    }
}
