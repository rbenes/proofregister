﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProofRegister.DAL.Entities
{
    [ComplexType]
    public class AddressDal : ComplexPropertyBase
    {
        [StringLength(128)]
        public string Street { get; set; }

        [StringLength(128)]
        public string City { get; set; }

        [StringLength(10)]
        public string PostCode { get; set; }

        public override bool HasValue => Street != null || City != null || PostCode != null;

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(Street)}: {Street}, {nameof(City)}: {City}, {nameof(PostCode)}: {PostCode}";
        }
    }
}
