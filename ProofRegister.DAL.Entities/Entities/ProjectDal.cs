﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProofRegister.DAL.Entities
{
    [Table("Projects")]
    public class ProjectDal : EntityBaseDal
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(128)]
        public string Name { get; set; }

        public DateTime? ValidFrom { get; set; }

        public DateTime? ValidTo { get; set; }

        public ICollection<ProjectPeriodDal> Periods { get; set; }

        public ICollection<ProjectPointDal> Points { get; set; }

        public ICollection<TrackingDeviceDal> Devices { get; set; }

        public ProjectDal()
        {
            Periods = new List<ProjectPeriodDal>();
            Points = new List<ProjectPointDal>();
            Devices = new List<TrackingDeviceDal>();
        }
    }
}
