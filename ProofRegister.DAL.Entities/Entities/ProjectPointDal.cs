﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProofRegister.DAL.Entities
{
    public abstract class ProjectPointDal : EntityBaseDal
    {
        [Required]
        public long ProjectId { get; set; }

        [ForeignKey(nameof(ProjectId))]
        public ProjectDal Project { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(128)]
        public string Name { get; set; }

        public PositionDal Position { get; set; }

        [Required]
        public decimal Radius { get; set; }

        [ForeignKey(nameof(ProjectPointTypeId))]
        public ProjectPointTypeDal ProjectPointType { get; set; }

        [Required]
        public long ProjectPointTypeId { get; set; }

        public AddressDal Address { get; set; }

        protected ProjectPointDal()
        {
            Address = new AddressDal();
            Position = new PositionDal();
        }
    }
}
