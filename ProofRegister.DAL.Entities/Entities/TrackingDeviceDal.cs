﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProofRegister.DAL.Entities
{
    [Table("TrackingDevices")]
    public class TrackingDeviceDal : EntityBaseDal
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(128)]
        public string KudyId { get; set; }

        [Required]
        public bool Enabled { get; set; }
    }
}
