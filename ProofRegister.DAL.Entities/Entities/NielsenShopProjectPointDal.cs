﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProofRegister.DAL.Entities
{
    [Table("NielsenShopProjectPoints")]
    public class NielsenShopProjectPointDal : ProjectPointDal
    {
        [StringLength(128)]
        public string Ico { get; set; }

        [StringLength(128)]
        public string Dic { get; set; }

        [StringLength(128)]
        public string IdNielsen { get; set; }

        [StringLength(128)]
        public string StoreCode { get; set; }
    }
}
