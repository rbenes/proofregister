﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TrackableEntities;

namespace ProofRegister.DAL.Entities
{
    public abstract class EntityBaseDal : ITrackable, IEntityBaseDal
    {
        /// <summary>
        /// Autor vytvoření záznamu.
        /// </summary>
        [StringLength(128)]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Datum vytvoření záznamu.
        /// </summary>
        public DateTime? CreatedWhen { get; set; }

        /// <summary>
        /// Primarni klic.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long? Id { get; set; }

        /// <summary>
        /// Autor změny záznamu.
        /// </summary>
        [StringLength(128)]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Datum změny záznamu.
        /// </summary>
        public DateTime? ModifiedWhen { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        /// <summary>
        /// Properties on an entity that have been modified.
        /// </summary>
        [NotMapped]
        ICollection<string> ITrackable.ModifiedProperties { get; set; }

        /// <summary>
        /// Change-tracking state of an entity.
        /// </summary>
        [NotMapped]
        TrackingState ITrackable.TrackingState { get; set; }

        public override string ToString()
        {
            return $"{GetType().Name} '[Id: {Id?.ToString() ?? "<null>"}]";
        }
    }
}
