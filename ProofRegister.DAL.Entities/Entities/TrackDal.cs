﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProofRegister.DAL.Entities
{
    [Table("Tracks")]
    public class TrackDal : EntityBaseDal
    {
        public DateTime ImportTime { get; set; }

        [ForeignKey(nameof(TrackStatusId))]
        public TrackStatusDal TrackStatus { get; set; }

        [Required]
        public long TrackStatusId { get; set; }

        public decimal Distance { get; set; }

        public ICollection<TrackSegmentDal> Segments { get; set; }

        public TrackDal()
        {
            Segments = new List<TrackSegmentDal>();
        }
    }
}
