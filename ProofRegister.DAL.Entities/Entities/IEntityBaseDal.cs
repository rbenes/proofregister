﻿using System;

namespace ProofRegister.DAL.Entities
{
    public interface IEntityBaseDal
    {
        string CreatedBy { get; set; }
        DateTime? CreatedWhen { get; set; }
        long? Id { get; set; }
        string ModifiedBy { get; set; }
        DateTime? ModifiedWhen { get; set; }
        byte[] RowVersion { get; set; }

        string ToString();
    }
}
