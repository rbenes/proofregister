﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProofRegister.DAL.Entities
{
    [Table("ProjectPointVisits")]
    public class ProjectPointVisitDal : EntityBaseDal
    {
        [Required]
        public long ProjectPointId { get; set; }

        [ForeignKey(nameof(ProjectPeriodId))]
        public ProjectPointDal ProjectPoint { get; set; }

        [Required]
        public long ProjectPeriodId { get; set; }

        [ForeignKey(nameof(ProjectPeriodId))]
        public ProjectPeriodDal ProjectPeriod { get; set; }

        [Required]
        public long TrackId { get; set; }

        [ForeignKey(nameof(TrackId))]
        public TrackDal Track { get; set; }

        public VisitInfoDal RealTrackInfo { get; set; }

        public VisitInfoDal ProtocolInfo { get; set; }

        public string ProjectPointInfo { get; set; }

        public ProjectPointVisitDal()
        {
            RealTrackInfo = new VisitInfoDal();
            ProtocolInfo = new VisitInfoDal();
        }
    }
}
