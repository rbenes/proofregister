﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProofRegister.DAL.Entities
{
    [ComplexType]
    public class VisitInfoDal : ComplexPropertyBase
    {
        public DateTime EntryTime { get; set; }

        public PositionDal EntryPosition { get; set; }

        public DateTime ExitTime { get; set; }

        public PositionDal ExitPosition { get; set; }

        public override bool HasValue => EntryTime != default(DateTime) || EntryPosition.HasValue || ExitTime != default(DateTime) || ExitPosition.HasValue;

        public VisitInfoDal()
        {
            EntryPosition = new PositionDal();
            ExitPosition = new PositionDal();
        }
    }
}
