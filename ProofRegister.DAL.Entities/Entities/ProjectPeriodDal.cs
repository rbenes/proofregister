﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProofRegister.DAL.Entities
{
    [Table("ProjectPeriods")]
    public class ProjectPeriodDal : EntityBaseDal
    {
        [Required]
        public long ProjectId { get; set; }

        [ForeignKey(nameof(ProjectId))]
        public ProjectDal Project { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(128)]
        public string Name { get; set; }

        public DateTime? ValidFrom { get; set; }

        public DateTime? ValidTo { get; set; }
    }
}
