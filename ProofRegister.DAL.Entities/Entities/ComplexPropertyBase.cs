﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProofRegister.DAL.Entities
{
    public abstract class ComplexPropertyBase
    {
        /// <summary>
        ///     Returns true if all properties are not null.
        /// </summary>
        [NotMapped]
        public abstract bool HasValue { get; }

        /// <summary>
        ///     Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return $"{GetType().Name} '[HasValue: {HasValue}]";
        }
    }
}
