﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProofRegister.DAL.Entities
{
    [Table("TrackSegments")]
    public class TrackSegmentDal : EntityBaseDal
    {
        [Required]
        public long TrackId { get; set; }

        [ForeignKey(nameof(TrackId))]
        public TrackDal Track { get; set; }

        public PositionDal Position { get; set; }
        
        public decimal? Elevation { get; set; }

        [Required]
        public DateTime SegmentTime { get; set; }

        public TrackSegmentDal()
        {
            Position = new PositionDal();
        }
    }
}
