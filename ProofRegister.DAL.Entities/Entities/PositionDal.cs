﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProofRegister.DAL.Entities
{
    [ComplexType]
    public class PositionDal : ComplexPropertyBase
    {
        [Required]
        public decimal Latitude { get; set; }

        [Required]
        public decimal Longitude { get; set; }

        public override bool HasValue => Latitude != default(decimal) || Longitude != default(decimal);
    }
}
