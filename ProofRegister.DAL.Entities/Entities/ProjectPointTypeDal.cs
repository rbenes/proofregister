﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProofRegister.DAL.Entities
{
    [Table("ProjectPointTypes")]
    public class ProjectPointTypeDal : EntityBaseDal
    {
        [Required]
        public long ProjectId { get; set; }

        [ForeignKey(nameof(ProjectId))]
        public ProjectDal Project { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(128)]
        public string Name { get; set; }
    }
}
