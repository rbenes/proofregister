﻿namespace ProofRegister.Web.Infrastructure
{
    public interface IWebSettings
    {
        string ServiceUrl { get; }
        
        string BaseWebApiUrl { get; }
    }
}
