﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProofRegister.Contracts.Entities;

namespace ProofRegister.Web.Infrastructure.WebApiClients
{
    public interface ITrackSegmentWebApiClient : IWebApiClient<TrackSegmentDto>
    {
        Task<IEnumerable<TrackSegmentDto>> GetTrackSegmentsForTrack(Guid corrId, long trackId);
    }
}
