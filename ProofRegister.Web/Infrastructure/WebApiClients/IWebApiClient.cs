﻿using System;
using System.Threading.Tasks;
using ProofRegister.Contracts.Entities;
using ProofRegister.Contracts.Entities.Responses;

namespace ProofRegister.Web.Infrastructure.WebApiClients
{
    public interface IWebApiClient<T> where T : EntityBaseDto
    {
        /// <summary>
        /// Create entity.
        /// </summary>
        /// <param name="corrId">Correlation Id</param>
        /// <param name="entity">Entity instance</param>
        Task<T> CreateAsync(Guid corrId, T entity);

        /// <summary>
        /// Loads all entities.
        /// </summary>
        /// <param name="corrId">Correlation Id</param>
        /// <param name="page">Page - zero based</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>List of entities</returns>
        Task<PagedEntitiesResponse<T>> GetAllAsync(Guid corrId, int? page, int? pageSize);

        /// <summary>
        /// Loads single entity.
        /// </summary>
        /// <param name="corrId">Correlation Id</param>
        /// <param name="id">Entity Id</param>
        /// <returns>Entity</returns>
        Task<T> GetSingleAsync(Guid corrId, long id);

        /// <summary>
        /// Remove entity.
        /// </summary>
        /// <param name="corrId">Correlation Id</param>
        /// <param name="id">Entity Id</param>
        Task RemoveAsync(Guid corrId, long id);

        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="corrId">Correlation Id</param>
        /// <param name="entity">Entity instance</param>
        Task<T> UpdateAsync(Guid corrId, T entity);
    }
}
