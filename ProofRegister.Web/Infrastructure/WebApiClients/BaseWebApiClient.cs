﻿using System;
using System.Data.Entity.Core;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using ProofRegister.Common.Exceptions;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Contracts;
using ProofRegister.Contracts.Entities;
using ProofRegister.Contracts.Entities.Responses;
using ProofRegister.Web.Common;
using Constants = ProofRegister.Common.Constants;

namespace ProofRegister.Web.Infrastructure.WebApiClients
{
    public class BaseWebApiClient<T> : IWebApiClient<T> where T : EntityBaseDto
    {
        #region Constructors and Destructors

        protected BaseWebApiClient(ILogger logger, IWebSettings webSettings)
        {
            if (webSettings == null) throw new ArgumentNullException(nameof(webSettings));
            BaseUrl = $"{webSettings.BaseWebApiUrl}/{WebApiCommon.GetServiceName(typeof(T))}";
            ServiceUrl = webSettings.ServiceUrl;

            Logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion

        #region Protected and Private Methods

        protected HttpClient CreateHttpClient(Guid corrId)
        {
            var client = new HttpClient(new HttpClientHandler {UseDefaultCredentials = true}) {BaseAddress = new Uri(ServiceUrl)};
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add(Constants.HttpCorrelationIdHeader, corrId.ToString());
            client.DefaultRequestHeaders.Add(Constants.HttpUsernameHeader, HttpContext.Current?.User?.Identity?.Name);
            return client;
        }

        #endregion

        #region Fields

        internal readonly string BaseUrl;
        internal string ServiceUrl;
        protected readonly ILogger Logger;

        #endregion

        #region IWebApiClient<T> Implementation

        public async Task<T> CreateAsync(Guid corrId, T entity)
        {
            using (var client = CreateHttpClient(corrId))
            {
                var response = await client.PostAsJsonAsync(BaseUrl, entity).ConfigureAwait(false);
                Logger.WriteVerbose($"WebApi Call: {response.RequestMessage.RequestUri}");

                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var serverResponse = await response.Content.ReadAsAsync<ValidationErrorsResponse>().ConfigureAwait(false);
                        if (serverResponse?.Messages != null && serverResponse.Messages.Any())
                            throw EntityValidationException.Create(serverResponse.Messages);
                    }
                    throw new UpdateException();
                }

                return await response.Content.ReadAsAsync<T>().ConfigureAwait(false);
            }
        }

        public async Task<PagedEntitiesResponse<T>> GetAllAsync(Guid corrId, int? page, int? pageSize)
        {
            using (var client = CreateHttpClient(corrId))
            {
                var response = await client.GetAsync($"{BaseUrl}{Tools.CreateUrlParameters(page, pageSize)}").ConfigureAwait(false);
                Logger.WriteVerbose($"WebApi Call: {response.RequestMessage.RequestUri}");
                if (!response.IsSuccessStatusCode)
                    return new PagedEntitiesResponse<T>();

                return await response.Content.ReadAsAsync<PagedEntitiesResponse<T>>().ConfigureAwait(false);
            }
        }

        public async Task<T> GetSingleAsync(Guid corrId, long id)
        {
            using (var client = CreateHttpClient(corrId))
            {
                var response = await client.GetAsync($"{BaseUrl}/{id}").ConfigureAwait(false);
                Logger.WriteVerbose($"WebApi Call: {response.RequestMessage.RequestUri}");
                if (!response.IsSuccessStatusCode)
                    return null;

                return await response.Content.ReadAsAsync<T>().ConfigureAwait(false);
            }
        }

        public async Task RemoveAsync(Guid corrId, long id)
        {
            using (var client = CreateHttpClient(corrId))
            {
                var response = await client.DeleteAsync($"{BaseUrl}/{id}").ConfigureAwait(false);
                Logger.WriteVerbose($"WebApi Call: {response.RequestMessage.RequestUri}");

                if (response.StatusCode != HttpStatusCode.NoContent)
                    throw new UpdateException();
            }
        }

        public async Task<T> UpdateAsync(Guid corrId, T entity)
        {
            using (var client = CreateHttpClient(corrId))
            {
                var response = await client.PutAsJsonAsync($"{BaseUrl}/{entity.Id}", entity).ConfigureAwait(false);
                Logger.WriteVerbose($"WebApi Call: {response.RequestMessage.RequestUri}");
                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsAsync<T>().ConfigureAwait(false);

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    var serverResponse = await response.Content.ReadAsAsync<ValidationErrorsResponse>().ConfigureAwait(false);
                    if (serverResponse?.Messages != null && serverResponse.Messages.Any())
                        throw EntityValidationException.Create(serverResponse.Messages);
                }

                if (response.StatusCode == HttpStatusCode.Conflict)
                    throw new ConcurrencyException();

                throw new UpdateException();
            }
        }

        #endregion
    }
}
