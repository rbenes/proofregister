﻿using System;
using System.Web;

namespace ProofRegister.Web.Infrastructure
{
    public interface ICorrelationIdProvider
    {
        /// <summary>
        ///     Create cllient correlation id.
        /// </summary>
        /// <param name="httpContext">Http context from controlleru</param>
        /// <returns>Guid</returns>
        Guid GetCorrelationId(HttpContextBase httpContext);
    }
}
