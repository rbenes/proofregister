﻿using AutoMapper;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Common
{
    public class ProofRegisterWebProfile : Profile
    {
        public ProofRegisterWebProfile()
        {
            CreateMap<AddressVm, AddressDto>();
            CreateMap<AddressDto, AddressVm>();
            
            CreateMap<NielsenShopProjectPointVm, NielsenShopProjectPointDto>();
            CreateMap<NielsenShopProjectPointDto, NielsenShopProjectPointVm>();

            CreateMap<PositionVm, PositionDto>();
            CreateMap<PositionDto, PositionVm>();

            CreateMap<ProjectVm, ProjectDto>();
            CreateMap<ProjectDto, ProjectVm>();

            CreateMap<ProjectPeriodVm, ProjectPeriodDto>();
            CreateMap<ProjectPeriodDto, ProjectPeriodVm>();

            CreateMap<ProjectPointVm, ProjectPointDto>();
            CreateMap<ProjectPointDto, ProjectPointVm>();

            CreateMap<ProjectPointTypeVm, ProjectPointTypeDto>();
            CreateMap<ProjectPointTypeDto, ProjectPointTypeVm>();

            CreateMap<ProjectPointVisitVm, ProjectPointVisitDto>();
            CreateMap<ProjectPointVisitDto, ProjectPointVisitVm>();

            CreateMap<TrackVm, TrackDto>();
            CreateMap<TrackDto, TrackVm>();

            CreateMap<TrackingDeviceVm, TrackingDeviceDto>();
            CreateMap<TrackingDeviceDto, TrackingDeviceVm>();

            CreateMap<TrackSegmentVm, TrackSegmentDto>();
            CreateMap<TrackSegmentDto, TrackSegmentVm>();

            CreateMap<TrackStatusVm, TrackStatusDto>();
            CreateMap<TrackStatusDto, TrackStatusVm>();

            CreateMap<VisitInfoVm, VisitInfoDto>();
            CreateMap<VisitInfoDto, VisitInfoVm>();
        }
    }
}
