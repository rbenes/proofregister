﻿using System.Collections.Generic;
using PagedList;

namespace ProofRegister.Web.Common
{
    /// <summary>
    /// Create empty instance of PagedList.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EmptyPagedList<T> : PagedList<T>
    {
        public EmptyPagedList() : base(new List<T>(), 1, 1)
        {
        }
    }
}
