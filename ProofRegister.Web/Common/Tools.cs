﻿using System.Collections.Generic;
using System.Linq;

namespace ProofRegister.Web.Common
{
    public static class Tools
    {
        /// <summary>
        /// Geenrates URL parameters.
        /// </summary>
        /// <param name="pageSize">Page Size</param>
        /// <param name="page">Zero based page</param>
        /// <param name="searchQuery">Search query</param>
        /// <returns>URL string</returns>
        public static string CreateUrlParameters(int? page, int? pageSize, string searchQuery = null)
        {
            var items = new List<string>();
            if (page != null)
                items.Add($"{nameof(page)}={page}");

            if (pageSize != null)
                items.Add($"{nameof(pageSize)}={pageSize}");

            if (searchQuery != null)
                items.Add($"{nameof(searchQuery)}={searchQuery}");

            if (items.Any())
                return $"?{string.Join("&", items)}";

            return "";
        }
    }
}
