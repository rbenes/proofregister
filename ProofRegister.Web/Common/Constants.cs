﻿namespace ProofRegister.Web.Common
{
    public static class Constants
    {
        /// <summary>
        ///     Key for identifing error message from TempData.
        /// </summary>
        public const string ErrorMessage = "ErrorMessage";

        /// <summary>
        ///     Key for identifing success message from TempData.
        /// </summary>
        public const string OkMessage = "OkMessage";

        /// <summary>
        ///     Count of records on page.
        /// </summary>
        public const int PageSize = 20;

        /// <summary>
        ///     Key for identifing validation message from TempData.
        /// </summary>
        public const string ValidationMessages = "ValidationMessages";
    }
}
