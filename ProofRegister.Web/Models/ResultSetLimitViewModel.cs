﻿namespace ProofRegister.Web.Models
{
    public class ResultSetLimitViewModel
    {
        public int Count { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }
    }
}
