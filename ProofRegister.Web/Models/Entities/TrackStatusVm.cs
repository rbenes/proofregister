﻿namespace ProofRegister.Web.Models.Entities
{
    public class TrackStatusVm : EntityBaseVm
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
