﻿namespace ProofRegister.Web.Models.Entities
{
    public class TrackingDeviceVm : EntityBaseVm
    {
        public string KudyId { get; set; }

        public bool Enabled { get; set; }
    }
}
