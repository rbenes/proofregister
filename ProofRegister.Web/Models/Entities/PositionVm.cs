﻿namespace ProofRegister.Web.Models.Entities
{
    public class PositionVm
    {
        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }
    }
}
