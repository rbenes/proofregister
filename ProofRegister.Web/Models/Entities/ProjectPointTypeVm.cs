﻿namespace ProofRegister.Web.Models.Entities
{
    public class ProjectPointTypeVm : EntityBaseVm
    {
        public long ProjectId { get; set; }

        public ProjectVm Project { get; set; }

        public string Name { get; set; }
    }
}
