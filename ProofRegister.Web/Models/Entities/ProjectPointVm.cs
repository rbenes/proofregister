﻿namespace ProofRegister.Web.Models.Entities
{
    public class ProjectPointVm : EntityBaseVm
    {
        public long ProjectId { get; set; }

        public ProjectVm Project { get; set; }

        public string Name { get; set; }

        public PositionVm Position { get; set; }

        public decimal Radius { get; set; }

        public ProjectPointTypeVm ProjectPointType { get; set; }

        public long ProjectPointTypeId { get; set; }

        public AddressVm Address { get; set; }

        protected ProjectPointVm()
        {
            Position = new PositionVm();
            Address = new AddressVm();
        }
    }
}
