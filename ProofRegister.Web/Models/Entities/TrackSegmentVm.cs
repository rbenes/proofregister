﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProofRegister.Web.Models.Entities
{
    public class TrackSegmentVm : EntityBaseVm
    {
        public TrackSegmentVm()
        {
            Position = new PositionVm();
        }

        public long TrackId { get; set; }

        public TrackVm Track { get; set; }

        public PositionVm Position { get; set; }

        public decimal? Elevation { get; set; }

        public DateTime SegmentTime { get; set; }
    }
}
