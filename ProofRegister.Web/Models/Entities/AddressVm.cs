﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProofRegister.Web.Models.Entities
{
    public class AddressVm
    {
        public string Street { get; set; }

        public string City { get; set; }

        public string PostCode { get; set; }

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(Street)}: {Street}, {nameof(City)}: {City}, {nameof(PostCode)}: {PostCode}";
        }
    }
}
