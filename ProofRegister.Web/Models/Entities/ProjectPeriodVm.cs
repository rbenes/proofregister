﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProofRegister.Web.Models.Entities
{
    public class ProjectPeriodVm : EntityBaseVm
    {
        public long ProjectId { get; set; }

        public ProjectVm Project { get; set; }

        public string Name { get; set; }

        public DateTime? ValidFrom { get; set; }

        public DateTime? ValidTo { get; set; }
    }
}
