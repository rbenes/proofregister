﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProofRegister.Web.Models.Entities
{
    public class ProjectVm : EntityBaseVm
    {
        public ProjectVm()
        {
            Periods = new List<ProjectPeriodVm>();
            Points = new List<ProjectPointVm>();
            Devices = new List<TrackingDeviceVm>();
        }

        public string Name { get; set; }

        public DateTime? ValidFrom { get; set; }

        public DateTime? ValidTo { get; set; }

        public ICollection<ProjectPeriodVm> Periods { get; set; }

        public ICollection<ProjectPointVm> Points { get; set; }

        public ICollection<TrackingDeviceVm> Devices { get; set; }
    }
}
