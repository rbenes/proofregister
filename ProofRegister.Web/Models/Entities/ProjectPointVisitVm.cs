﻿namespace ProofRegister.Web.Models.Entities
{
    public class ProjectPointVisitVm : EntityBaseVm
    {
        public ProjectPointVisitVm()
        {
            RealTrackInfo = new VisitInfoVm();
            ProtocolInfo = new VisitInfoVm();
        }

        public long ProjectPointId { get; set; }

        public ProjectPointVm ProjectPoint { get; set; }

        public long ProjectPeriodId { get; set; }

        public ProjectPeriodVm ProjectPeriod { get; set; }

        public long TrackId { get; set; }

        public TrackVm Track { get; set; }

        public VisitInfoVm RealTrackInfo { get; set; }

        public VisitInfoVm ProtocolInfo { get; set; }

        public string ProjectPointInfo { get; set; }
    }
}
