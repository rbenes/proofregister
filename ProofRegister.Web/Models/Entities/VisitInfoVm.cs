﻿using System;

namespace ProofRegister.Web.Models.Entities
{
    public class VisitInfoVm
    {
        public VisitInfoVm()
        {
            EntryPosition = new PositionVm();
            ExitPosition = new PositionVm();
        }

        public DateTime EntryTime { get; set; }

        public PositionVm EntryPosition { get; set; }

        public DateTime ExitTime { get; set; }

        public PositionVm ExitPosition { get; set; }
    }
}
