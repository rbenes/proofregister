﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProofRegister.Web.Models.Entities
{
    public class TrackVm : EntityBaseVm
    {
        public TrackVm()
        {
            Segments = new List<TrackSegmentVm>();
        }

        public DateTime ImportTime { get; set; }

        public TrackStatusVm TrackStatus { get; set; }

        public long TrackStatusId { get; set; }

        public decimal Distance { get; set; }

        public ICollection<TrackSegmentVm> Segments { get; set; }
    }
}
