﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProofRegister.Web.Models.Entities
{
    public class NielsenShopProjectPointVm : ProjectPointVm
    {
        public string Ico { get; set; }

        public string Dic { get; set; }

        public string IdNielsen { get; set; }

        public string StoreCode { get; set; }
    }
}
