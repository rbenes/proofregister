﻿using System.Collections.Generic;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Models
{
    public class PagedEntitiesResponseViewModel<T> where T : EntityBaseVm
    {
        public ResultSetLimitViewModel ResultSetLimit { get; set; }

        public IEnumerable<T> Entities { get; set; }
    }
}
