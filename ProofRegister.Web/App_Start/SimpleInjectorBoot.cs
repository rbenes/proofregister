﻿using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Owin;
using ProofRegister.Common;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Web.Business;
using ProofRegister.Web.Common;
using ProofRegister.Web.Infrastructure;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;

namespace ProofRegister.Web
{
    public class SimpleInjectorBoot
    {
        public static void SimpleInjectorSetup(IAppBuilder appBuilder, ILogger logger)
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            //container.RegisterInitializer<HandlerBase>(handlerToInitialize => { handlerToInitialize.ExecuteAsynchronously = true; });

            var config = new MapperConfiguration(cfg => { cfg.AddProfile<ProofRegisterWebProfile>(); });
            config.AssertConfigurationIsValid();

            var dbContext = new ApplicationDbContext();

            container.Register(() => logger, Lifestyle.Singleton);
            container.Register(() => dbContext, Lifestyle.Scoped);
            container.Register<IUserStore<ApplicationUser>>(() => new UserStore<ApplicationUser>(dbContext), Lifestyle.Scoped);
            container.Register<IAuthenticationManager>(() => new OwinContext(new Dictionary<string, object>()).Authentication, Lifestyle.Scoped);
            container.Register<ApplicationUserManager>(Lifestyle.Scoped);
            container.Register<ApplicationSignInManager>(Lifestyle.Scoped);
            container.Register<IdentityFactoryOptions<ApplicationUserManager>>(Lifestyle.Scoped);
            container.Register<ICorrelationIdProvider, CorrelationIdProvider>();
            container.Register<IWebSettings, WebSettings>();
            container.Register<IExceptionHandling, ExceptionHandling>();
            container.Register<IQueryProcessor, QueryProcessor>();
            container.Register<ICommandProcessor, CommandProcessor>();
            container.Register(typeof(ICommandHandler<>), new[] { typeof(SimpleInjectorBoot).Assembly });
            container.Register(typeof(IQueryHandler<,>), new[] { typeof(SimpleInjectorBoot).Assembly });
            container.Register(typeof(IWebApiClient<>), new[] { typeof(IWebApiClient<>).Assembly });
            container.RegisterSingleton(config);
            container.Register(() => config.CreateMapper(container.GetInstance));

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.RegisterMvcIntegratedFilterProvider();
            container.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
            appBuilder.CreatePerOwinContext(() => container.GetInstance<ApplicationUserManager>());
        }
    }
}
