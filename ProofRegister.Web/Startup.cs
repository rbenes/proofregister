﻿using Microsoft.Owin;
using Owin;
using ProofRegister.Common;
using ProofRegister.Common.Infrastructure;

[assembly: OwinStartupAttribute(typeof(ProofRegister.Web.Startup))]
namespace ProofRegister.Web
{
    public partial class Startup
    {
        private readonly ILogger logger = new Logger();

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            SimpleInjectorBoot.SimpleInjectorSetup(app, logger);
        }
    }
}
