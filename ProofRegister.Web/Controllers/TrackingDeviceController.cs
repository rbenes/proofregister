﻿using System.Web.Mvc;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Web.Infrastructure;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Controllers
{
    public class TrackingDeviceController : BaseCrudController<TrackingDeviceVm>
    {
        public TrackingDeviceController(IExceptionHandling exceptionHandling,
            ICommandProcessor commandProcessor,
            IQueryProcessor queryProcessor,
            ICorrelationIdProvider correlationIdProvider)
            : base(exceptionHandling, commandProcessor, queryProcessor, correlationIdProvider)
        {
        }

        // GET: TrackingDevice/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
    }
}
