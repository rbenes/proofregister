﻿using ProofRegister.Common.Infrastructure;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Web.Infrastructure;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Controllers
{
    public class ProjectController : BaseCrudController<ProjectVm>
    {
        public ProjectController(IExceptionHandling exceptionHandling, ICommandProcessor commandProcessor, IQueryProcessor queryProcessor, ICorrelationIdProvider correlationIdProvider)
            : base(exceptionHandling, commandProcessor, queryProcessor, correlationIdProvider)
        {
        }
    }
}
