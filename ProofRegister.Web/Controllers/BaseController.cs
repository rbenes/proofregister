﻿using System;
using System.Web.Mvc;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Web.Common;
using ProofRegister.Web.Infrastructure;

namespace ProofRegister.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        private readonly ICorrelationIdProvider correlationIdProvider;
        protected readonly IExceptionHandling exceptionHandling;

        protected BaseController(IExceptionHandling exceptionHandling, ICorrelationIdProvider correlationIdProvider)
        {
            this.exceptionHandling = exceptionHandling;
            this.correlationIdProvider = correlationIdProvider;
        }

        protected Guid GetCid()
        {
            return correlationIdProvider.GetCorrelationId(ControllerContext?.HttpContext);
        }

        protected ActionResult RedirectToActionAfterException(string actionName, long viewModelId, Exception exception)
        {
            HandleControllerException(exception);
            return Redirect(Url.Action(actionName, new {id = viewModelId}));
        }

        protected void HandleControllerException(Exception ex)
        {
            exceptionHandling.HandleException(ex);
            TempData[Constants.ErrorMessage] = ex.Message;
        }
    }
}
