﻿using System.Web.Mvc;

namespace ProofRegister.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
