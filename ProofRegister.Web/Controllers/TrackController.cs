﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Web.Infrastructure;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Controllers
{
    public class TrackController : BaseCrudController<TrackVm>
    {
        public TrackController(IExceptionHandling exceptionHandling, ICommandProcessor commandProcessor, IQueryProcessor queryProcessor, ICorrelationIdProvider correlationIdProvider)
            : base(exceptionHandling, commandProcessor, queryProcessor, correlationIdProvider)
        {
        }

        public async Task<IList<TrackSegmentVm>> ShowMap(long id)
        {
            throw new NotImplementedException();
            //var trackSegments = await this.Orchestration.ShowMapAsync(id);
            //return trackSegments;
        }
    }
}
