﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using PagedList;
using ProofRegister.Common.Exceptions;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Web.Business.Commands;
using ProofRegister.Web.Business.Queries;
using ProofRegister.Web.Common;
using ProofRegister.Web.Infrastructure;
using ProofRegister.Web.Models;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Controllers
{
    public abstract class BaseCrudController<T> : BaseController
        where T : EntityBaseVm, new()
    {
        private readonly string indexView = "Index";
        protected readonly ICommandProcessor CommandProcessor;
        protected readonly IQueryProcessor QueryProcessor;

        protected BaseCrudController(IExceptionHandling exceptionHandling, ICommandProcessor commandProcessor, IQueryProcessor queryProcessor, ICorrelationIdProvider correlationIdProvider)
            : base(exceptionHandling, correlationIdProvider)
        {
            QueryProcessor = queryProcessor;
            CommandProcessor = commandProcessor;
        }

        // GET: base/Create
        public ActionResult Create()
        {
            return View(new T());
        }

        // POST: base/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind] T viewModel)
        {
            if (!ModelState.IsValid)
                return View(viewModel);

            try
            {
                var cmd = new ControllerCreateCommand<T>(viewModel, "");
                await CommandProcessor.ProcessAsync(cmd, CancellationToken.None);
            }
            catch (ValidationException valEx)
            {
                TempData[Constants.ValidationMessages] = valEx.Errors;
                return View(viewModel);
            }
            catch (Exception ex)
            {
                exceptionHandling.HandleException(ex);
                return View(viewModel);
            }

            return RedirectToAction(indexView);
        }

        // GET: base/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            T viewModel;
            try
            {
                var query = new ControllerGetEntityQuery<T>(id.Value);
                viewModel = await QueryProcessor.ProcessAsync(query, CancellationToken.None);
            }
            catch (Exception ex)
            {
                exceptionHandling.HandleException(ex);
                return RedirectToAction(indexView);
            }
            if (viewModel == null)
                return HttpNotFound();

            return View(viewModel);
        }

        // POST: base/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                var cmd = new ControllerDeleteCommand<T>(id.Value);
                await CommandProcessor.ProcessAsync(cmd, CancellationToken.None);
            }
            catch (Exception ex)
            {
                return RedirectToActionAfterException("Delete", id.Value, ex);
            }
            return RedirectToAction(indexView);
        }

        // GET: base/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            T viewModel;
            try
            {
                var query = new ControllerGetEntityQuery<T>(id.Value);
                viewModel = await QueryProcessor.ProcessAsync(query, CancellationToken.None);
            }
            catch (Exception ex)
            {
                exceptionHandling.HandleException(ex);
                return RedirectToAction(indexView);
            }
            if (viewModel == null)
                return HttpNotFound();

            return View(viewModel);
        }

        // POST: base/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind] T viewModel)
        {
            if (!ModelState.IsValid)
                return View(viewModel);

            if (viewModel.Id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                var cmd = new ControllerUpdateCommand<T>(viewModel, "");
                await CommandProcessor.ProcessAsync(cmd, CancellationToken.None);
            }
            catch (ValidationException valEx)
            {
                TempData[Constants.ValidationMessages] = valEx.Errors;
                return View(viewModel);
            }
            catch (Exception ex)
            {
                HandleControllerException(ex);
                return View(viewModel);
            }
            return RedirectToAction(indexView);
        }

        // GET: base
        public async Task<ActionResult> Index(int? page)
        {
            try
            {
                var realPage = page ?? 1;
                var offset = Math.Max(0, realPage - 1);
                var query = new ControllerGetEntitiesQuery<PagedEntitiesResponseViewModel<T>>(offset, Constants.PageSize);
                var items = await QueryProcessor.ProcessAsync(query, CancellationToken.None);
                var totalCount = items.ResultSetLimit?.Count ?? 0;

                return View(new StaticPagedList<T>(items?.Entities ?? new List<T>(), realPage, Constants.PageSize, totalCount));
            }
            catch (Exception ex)
            {
                TempData[Constants.ErrorMessage] = ex.Message;
                return View(new EmptyPagedList<T>());
            }
        }
    }
}
