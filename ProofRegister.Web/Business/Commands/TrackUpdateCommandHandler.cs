﻿using AutoMapper;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Commands
{
    public class TrackUpdateCommandHandler : ControllerUpdateCommandHandler<TrackVm, TrackDto>
    {
        public TrackUpdateCommandHandler(IMapper mapper, IWebApiClient<TrackDto> webApiClient)
            : base(mapper, webApiClient)
        {
        }
    }
}
