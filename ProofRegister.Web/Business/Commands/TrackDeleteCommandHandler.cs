﻿using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Commands
{
    public class TrackDeleteCommandHandler : ControllerDeleteCommandHandler<TrackVm, TrackDto>
    {
        public TrackDeleteCommandHandler(IWebApiClient<TrackDto> webApiClient)
            : base(webApiClient)
        {
        }
    }
}
