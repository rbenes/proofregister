﻿using System.Threading;
using System.Threading.Tasks;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Commands
{
    public class ControllerDeleteCommandHandler<TViewModel, TDto> : ICommandHandler<ControllerDeleteCommand<TViewModel>>
        where TViewModel : EntityBaseVm
        where TDto : EntityBaseDto
    {
        private readonly IWebApiClient<TDto> webApiClient;

        public ControllerDeleteCommandHandler(IWebApiClient<TDto> webApiClient)
        {
            this.webApiClient = webApiClient;
        }

        public virtual async Task HandleAsync(ControllerDeleteCommand<TViewModel> command, CancellationToken cancellationToken)
        {
            await webApiClient.RemoveAsync(command.CommandId, command.Id);
        }
    }
}
