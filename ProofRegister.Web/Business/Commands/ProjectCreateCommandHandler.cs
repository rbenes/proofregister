﻿using AutoMapper;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Commands
{
    public class ProjectCreateCommandHandler : ControllerCreateCommandHandler<ProjectVm, ProjectDto>
    {
        public ProjectCreateCommandHandler(IMapper mapper, IWebApiClient<ProjectDto> webApiClient)
            : base(mapper, webApiClient)
        {
        }
    }
}
