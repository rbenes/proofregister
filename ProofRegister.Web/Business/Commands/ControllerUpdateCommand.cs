﻿using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Commands
{
    public class ControllerUpdateCommand<TViewModel> : CommandBase
        where TViewModel : EntityBaseVm
    {
        public ControllerUpdateCommand(TViewModel entity, string username)
        {
            Entity = entity;
            Username = username;
        }

        public TViewModel Entity { get; }

        public string Username { get; set; }
    }
}
