﻿using AutoMapper;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Commands
{
    public class TrackingDeviceCreateCommandHandler : ControllerCreateCommandHandler<TrackingDeviceVm, TrackingDeviceDto>
    {
        public TrackingDeviceCreateCommandHandler(IMapper mapper, IWebApiClient<TrackingDeviceDto> webApiClient)
            : base(mapper, webApiClient)
        {
        }
    }
}
