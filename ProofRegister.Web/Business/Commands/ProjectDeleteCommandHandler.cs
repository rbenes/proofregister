﻿using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Commands
{
    public class ProjectDeleteCommandHandler : ControllerDeleteCommandHandler<ProjectVm, ProjectDto>
    {
        public ProjectDeleteCommandHandler(IWebApiClient<ProjectDto> webApiClient)
            : base(webApiClient)
        {
        }
    }
}
