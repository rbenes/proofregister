﻿using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Commands
{
    public class TrackingDeviceDeleteCommandHandler : ControllerDeleteCommandHandler<TrackingDeviceVm, TrackingDeviceDto>
    {
        public TrackingDeviceDeleteCommandHandler(IWebApiClient<TrackingDeviceDto> webApiClient)
            : base(webApiClient)
        {
        }
    }
}
