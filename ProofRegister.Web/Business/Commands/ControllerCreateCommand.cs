﻿using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Commands
{
    public class ControllerCreateCommand<TViewModel> : CommandBase
        where TViewModel : EntityBaseVm
    {
        public ControllerCreateCommand(TViewModel mappedEntity, string username)
        {
            Entity = mappedEntity;
            Username = username;
        }

        public TViewModel Entity { get; }

        public string Username { get; }
    }
}
