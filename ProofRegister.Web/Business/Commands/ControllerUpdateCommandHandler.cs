﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Commands
{
    public class ControllerUpdateCommandHandler<TViewModel, TDto> : ICommandHandler<ControllerUpdateCommand<TViewModel>>
        where TViewModel : EntityBaseVm
        where TDto : EntityBaseDto
    {
        private readonly IWebApiClient<TDto> webApiClient;
        private readonly IMapper mapper;

        public ControllerUpdateCommandHandler(IMapper mapper, IWebApiClient<TDto> webApiClient)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.webApiClient = webApiClient ?? throw new ArgumentNullException(nameof(webApiClient));
        }

        public virtual async Task HandleAsync(ControllerUpdateCommand<TViewModel> command, CancellationToken cancellationToken)
        {
            var dtoEntity = mapper.Map<TViewModel, TDto>(command.Entity);
            await webApiClient.UpdateAsync(command.CommandId, dtoEntity);
        }
    }
}
