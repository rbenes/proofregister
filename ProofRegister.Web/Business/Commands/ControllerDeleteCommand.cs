﻿using System;
using ProofRegister.Common.Infrastructure.Commands;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Commands
{
    public class ControllerDeleteCommand<TViewModel> : CommandBase
        where TViewModel : EntityBaseVm
    {
        public ControllerDeleteCommand(TViewModel entity)
        {
            Id = entity?.Id ?? throw new ArgumentNullException(nameof(entity));
        }

        public ControllerDeleteCommand(long id)
        {
            Id = id;
        }

        public long Id { get; set; }
    }
}
