﻿using System;
using System.Diagnostics;
using System.Web;
using ProofRegister.Web.Infrastructure;

namespace ProofRegister.Web.Business
{
    public class CorrelationIdProvider : ICorrelationIdProvider
    {
        public Guid GetCorrelationId(HttpContextBase httpContext)
        {
            return Trace.CorrelationManager.ActivityId != Guid.Empty
                ? Trace.CorrelationManager.ActivityId
                : Guid.NewGuid();
        }
    }
}
