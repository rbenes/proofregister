﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Web.Infrastructure;

namespace ProofRegister.Web.Business
{
    public class WebSettings : IWebSettings
    {
        public string ServiceUrl { get; }
        public string BaseWebApiUrl { get; }

        public WebSettings(ILogger logger)
        {
            this.ServiceUrl = GetSetting<string>("ServiceUrl");
            this.BaseWebApiUrl = GetSetting<string>("BaseWebApiUrl");

            var items = new Dictionary<string, object>
            {
                {"ServiceUrl", this.ServiceUrl},
                {"BaseWebApiUrl", this.BaseWebApiUrl},
            };

            logger.WriteInfo($"Settings initialized.{Environment.NewLine}{String.Join(";", items.Select(kvp => $"{kvp.Key}: {kvp.Value}"))}");
        }
        

        private static T GetSetting<T>(string name)
        {
            string value = ConfigurationManager.AppSettings[name];

            if (value == null)
            {
                throw new Exception($"Could not find setting '{name}',");
            }

            return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
        }
    }
}
