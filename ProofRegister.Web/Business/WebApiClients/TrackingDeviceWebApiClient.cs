﻿using ProofRegister.Common.Infrastructure;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure;
using ProofRegister.Web.Infrastructure.WebApiClients;

namespace ProofRegister.Web.Business.WebApiClients
{
    public class TrackingDeviceWebApiClient : BaseWebApiClient<TrackingDeviceDto>
    {
        public TrackingDeviceWebApiClient(ILogger logger, IWebSettings webSettings)
            : base(logger, webSettings)
        {
        }
    }
}
