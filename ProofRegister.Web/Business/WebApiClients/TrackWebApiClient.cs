﻿using ProofRegister.Common.Infrastructure;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure;
using ProofRegister.Web.Infrastructure.WebApiClients;

namespace ProofRegister.Web.Business.WebApiClients
{
    public class TrackWebApiClient : BaseWebApiClient<TrackDto>
    {
        public TrackWebApiClient(ILogger logger, IWebSettings webSettings)
            : base(logger, webSettings)
        {
        }
    }
}
