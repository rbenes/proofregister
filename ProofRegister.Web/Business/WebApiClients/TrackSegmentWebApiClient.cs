﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ProofRegister.Common.Infrastructure;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure;
using ProofRegister.Web.Infrastructure.WebApiClients;

namespace ProofRegister.Web.Business.WebApiClients
{
    public class TrackSegmentWebApiClient : BaseWebApiClient<TrackSegmentDto>, ITrackSegmentWebApiClient
    {
        public TrackSegmentWebApiClient(ILogger logger, IWebSettings webSettings)
            : base(logger, webSettings)
        {
        }

        public async Task<IEnumerable<TrackSegmentDto>> GetTrackSegmentsForTrack(Guid corrId, long trackId)
        {
            using (var client = CreateHttpClient(corrId))
            {
                var response = await client.GetAsync($"{BaseUrl}/for-track/{trackId}").ConfigureAwait(false);
                Logger.WriteVerbose($"WebApi Call: {response.RequestMessage.RequestUri}");
                if (!response.IsSuccessStatusCode)
                    return new List<TrackSegmentDto>();

                return await response.Content.ReadAsAsync<IEnumerable<TrackSegmentDto>>().ConfigureAwait(false);
            }
        }
    }
}
