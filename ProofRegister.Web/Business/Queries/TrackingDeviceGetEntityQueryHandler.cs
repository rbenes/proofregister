﻿using AutoMapper;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Queries
{
    public class TrackingDeviceGetEntityQueryHandler : ControllerGetEntityQueryHandler<TrackingDeviceVm, TrackingDeviceDto>
    {
        public TrackingDeviceGetEntityQueryHandler(IWebApiClient<TrackingDeviceDto> webApiClient, IMapper mapper)
            : base(webApiClient, mapper)
        {
        }
    }
}
