﻿using ProofRegister.Common.Infrastructure.Queries;

namespace ProofRegister.Web.Business.Queries
{
    public class ControllerGetEntityQuery<TBusiness> : QueryBase<TBusiness>
    {
        public ControllerGetEntityQuery(long id)
        {
            Id = id;
        }

        public long Id { get; }
    }
}
