﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Contracts.Entities;
using ProofRegister.Contracts.Entities.Responses;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Queries
{
    public class ControllerGetEntitiesQueryHandler<TViewModel, TDto> : IQueryHandler<ControllerGetEntitiesQuery<PagedEntitiesResponseViewModel<TViewModel>>, PagedEntitiesResponseViewModel<TViewModel>>
        where TViewModel : EntityBaseVm
        where TDto : EntityBaseDto
    {
        private readonly IMapper mapper;
        private readonly IWebApiClient<TDto> webApiClient;

        public ControllerGetEntitiesQueryHandler(IWebApiClient<TDto> webApiClient, IMapper mapper)
        {
            this.mapper = mapper;
            this.webApiClient = webApiClient?? throw new ArgumentNullException(nameof(webApiClient));
        }

        public async Task<PagedEntitiesResponseViewModel<TViewModel>> HandleAsync(ControllerGetEntitiesQuery<PagedEntitiesResponseViewModel<TViewModel>> query, CancellationToken cancellationToken)
        {
            var entities = await webApiClient.GetAllAsync(query.QueryId, query.Page, query.PageSize);
            return mapper.Map<PagedEntitiesResponse<TDto>, PagedEntitiesResponseViewModel<TViewModel>>(entities);
        }
    }
}
