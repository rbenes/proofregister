﻿using System.Collections.Generic;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Queries
{
    public class TrackSegmentsForTrackQuery : QueryBase<IEnumerable<TrackSegmentVm>>
    {
        public long TrackId { get; }

        public TrackSegmentsForTrackQuery(long trackId)
        {
            TrackId = trackId;
        }
    }
}
