﻿using AutoMapper;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Queries
{
    public class TrackGetEntityQueryHandler : ControllerGetEntityQueryHandler<TrackVm, TrackDto>
    {
        public TrackGetEntityQueryHandler(IWebApiClient<TrackDto> webApiClient, IMapper mapper)
            : base(webApiClient, mapper)
        {
        }
    }
}
