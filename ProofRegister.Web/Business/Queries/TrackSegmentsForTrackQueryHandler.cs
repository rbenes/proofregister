﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Queries
{
    public class TrackSegmentsForTrackQueryHandler : IQueryHandler<TrackSegmentsForTrackQuery, IEnumerable<TrackSegmentVm>>
    {
        private readonly ITrackSegmentWebApiClient webApiClient;
        private readonly IMapper mapper;

        public TrackSegmentsForTrackQueryHandler(IMapper mapper, ITrackSegmentWebApiClient webApiClient)
        {
            this.webApiClient = webApiClient;
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<TrackSegmentVm>> HandleAsync(TrackSegmentsForTrackQuery query, CancellationToken cancellationToken)
        {
            var dtoEntites = await webApiClient.GetTrackSegmentsForTrack(query.QueryId, query.TrackId);
            return mapper.Map<IEnumerable<TrackSegmentDto>, IEnumerable<TrackSegmentVm>>(dtoEntites);
        }
    }
}
