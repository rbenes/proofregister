﻿using AutoMapper;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Queries
{
    public class ProjectGetEntitiesQueryHandler : ControllerGetEntitiesQueryHandler<ProjectVm, ProjectDto>
    {
        public ProjectGetEntitiesQueryHandler(IWebApiClient<ProjectDto> webApiClient, IMapper mapper)
            : base(webApiClient, mapper)
        {
        }
    }
}
