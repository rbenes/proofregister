﻿using AutoMapper;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Queries
{
    public class TrackGetEntitiesQueryHandler : ControllerGetEntitiesQueryHandler<TrackVm, TrackDto>
    {
        public TrackGetEntitiesQueryHandler(IWebApiClient<TrackDto> webApiClient, IMapper mapper)
            : base(webApiClient, mapper)
        {
        }
    }
}
