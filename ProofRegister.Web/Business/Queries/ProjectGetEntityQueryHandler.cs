﻿using AutoMapper;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Queries
{
    public class ProjectGetEntityQueryHandler : ControllerGetEntityQueryHandler<ProjectVm, ProjectDto>
    {
        public ProjectGetEntityQueryHandler(IWebApiClient<ProjectDto> webApiClient, IMapper mapper)
            : base(webApiClient, mapper)
        {
        }
    }
}
