﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ProofRegister.Common.Infrastructure.Queries;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Queries
{
    public class ControllerGetEntityQueryHandler<TViewModel, TDto> : IQueryHandler<ControllerGetEntityQuery<TViewModel>, TViewModel>
        where TViewModel : EntityBaseVm
        where TDto : EntityBaseDto
    {
        private readonly IWebApiClient<TDto> webApiClient;
        private readonly IMapper mapper;

        public ControllerGetEntityQueryHandler(IWebApiClient<TDto> webApiClient, IMapper mapper)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.webApiClient = webApiClient ?? throw new ArgumentNullException(nameof(webApiClient));
        }

        public async Task<TViewModel> HandleAsync(ControllerGetEntityQuery<TViewModel> query, CancellationToken cancellationToken)
        {
            var entity = await webApiClient.GetSingleAsync(query.QueryId, query.Id);
            return mapper.Map<TDto, TViewModel>(entity);
        }
    }
}
