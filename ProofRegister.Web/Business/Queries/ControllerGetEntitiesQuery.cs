﻿using ProofRegister.Common.Infrastructure.Queries;

namespace ProofRegister.Web.Business.Queries
{
    public class ControllerGetEntitiesQuery<TViewModel> : QueryBase<TViewModel>
    {
        public int? Page { get; }
        public int? PageSize { get; }

        public ControllerGetEntitiesQuery(int? page, int? pageSize)
        {
            Page = page;
            PageSize = pageSize;
        }
    }
}
