﻿using AutoMapper;
using ProofRegister.Contracts.Entities;
using ProofRegister.Web.Infrastructure.WebApiClients;
using ProofRegister.Web.Models.Entities;

namespace ProofRegister.Web.Business.Queries
{
    public class TrackingDeviceGetEntitiesQueryHandler : ControllerGetEntitiesQueryHandler<TrackingDeviceVm, TrackingDeviceDto>
    {
        public TrackingDeviceGetEntitiesQueryHandler(IWebApiClient<TrackingDeviceDto> webApiClient, IMapper mapper)
            : base(webApiClient, mapper)
        {
        }
    }
}
