﻿namespace ProofRegister.Common
{
    public class Constants
    {
        public const string HttpCorrelationIdHeader = "X-CorrId";

        /// <summary>
        /// ProofReg.
        /// </summary>
        public const string ConnectionStringName = "ProofReg";

        /// <summary>
        /// PR.
        /// </summary>
        public const string DatabaseSchema = "PR";

        /// <summary>
        /// Http header for username.
        /// </summary>
        public const string HttpUsernameHeader = "X-Username";
    }
}
