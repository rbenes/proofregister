﻿namespace ProofRegister.Common.Messages
{
    public class ExceptionMessages
    {
        public const string EntityValidationFailed = "Entity validation failed.";
    }
}
