﻿using System;
using ProofRegister.Common.Infrastructure;

namespace ProofRegister.Common
{
    public class ExceptionHandling : IExceptionHandling
    {
        private readonly ILogger logger;

        public ExceptionHandling(ILogger logger)
        {
            this.logger = logger;
        }

        public bool HandleException(Exception ex)
        {
            logger.WriteError(ex);
            return true;
        }

        public void CurrentDomainOnUnhandledException(object o, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            logger.WriteError(unhandledExceptionEventArgs.ExceptionObject as Exception);
        }
    }
}
