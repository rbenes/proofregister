﻿using System;

namespace ProofRegister.Common.Infrastructure
{
    public interface ILogger
    {
        void Write(string message);
        void WriteInfo(string message);
        void WriteVerbose(string message);
        void WriteWarning(string message);
        void WriteError(Exception exception);
        void WriteStart(string message);
        void WriteStop(string message);
    }
}
