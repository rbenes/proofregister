﻿using System;

namespace ProofRegister.Common.Infrastructure
{
    public interface IExceptionHandling
    {
        bool HandleException(Exception ex);

        void CurrentDomainOnUnhandledException(object o, UnhandledExceptionEventArgs unhandledExceptionEventArgs);
    }
}
