﻿using System;
using System.Threading;
using System.Threading.Tasks;
using SimpleInjector;

namespace ProofRegister.Common.Infrastructure.Queries
{
    public class QueryProcessor : IQueryProcessor
    {
        private readonly Container container;
        private readonly ILogger logger;

        public QueryProcessor(Container container, ILogger logger)
        {
            this.container = container ?? throw new ArgumentNullException(nameof(container));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<TResult> ProcessAsync<TResult>(IQuery<TResult> query, CancellationToken cancellationToken)
        {
            var handlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResult));

            dynamic handler = container.GetInstance(handlerType);

            var typeName = query.GetType().GetFriendlyName();
            try
            {
                logger.WriteVerbose($"Start processing query {typeName} ID {query.QueryId}.");
                var result = await handler.HandleAsync((dynamic) query, cancellationToken).ConfigureAwait(false);
                logger.WriteVerbose($"Finished processing query {typeName} ID {query.QueryId}.");
                return result;
            }
            catch (OperationCanceledException)
            {
                logger.WriteWarning($"Processing query {typeName} ID {query.QueryId} has been cancelled.");
                return default(TResult);
            }
        }
    }
}
