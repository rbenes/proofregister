﻿using System;

namespace ProofRegister.Common.Infrastructure.Queries
{
    // ReSharper disable once UnusedTypeParameter
    public interface IQuery<TResult>
    {
        Guid QueryId { get; }
    }
}
