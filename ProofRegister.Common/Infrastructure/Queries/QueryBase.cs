﻿using System;

namespace ProofRegister.Common.Infrastructure.Queries
{
    public abstract class QueryBase<TResult> : IQuery<TResult>
    {
        protected QueryBase()
        {
            QueryId = Guid.NewGuid();
        }

        public Guid QueryId { get; }
    }
}
