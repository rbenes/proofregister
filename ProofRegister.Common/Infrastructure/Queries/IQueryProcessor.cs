﻿using System.Threading;
using System.Threading.Tasks;

namespace ProofRegister.Common.Infrastructure.Queries
{
    public interface IQueryProcessor
    {
        Task<TResult> ProcessAsync<TResult>(IQuery<TResult> query, CancellationToken cancellationToken);
    }
}
