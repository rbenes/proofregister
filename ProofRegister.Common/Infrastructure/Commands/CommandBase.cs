﻿using System;

namespace ProofRegister.Common.Infrastructure.Commands
{
    public abstract class CommandBase : ICommand
    {
        protected CommandBase()
        {
            CommandId = Guid.NewGuid();
        }

        public Guid CommandId { get; }
    }
}
