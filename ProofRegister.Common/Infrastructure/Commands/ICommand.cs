﻿using System;

namespace ProofRegister.Common.Infrastructure.Commands
{
    public interface ICommand
    {
        Guid CommandId { get; }
    }
}
