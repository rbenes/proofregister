﻿using System.Threading;
using System.Threading.Tasks;

namespace ProofRegister.Common.Infrastructure.Commands
{
    public interface ICommandProcessor
    {
        void Enqueue<TCommand>(TCommand command, CancellationToken cancellationToken) where TCommand : ICommand;

        Task ProcessAsync<TCommand>(TCommand command, CancellationToken cancellationToken) where TCommand : ICommand;
    }
}
