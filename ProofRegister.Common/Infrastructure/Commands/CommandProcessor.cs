﻿using System;
using System.Threading;
using System.Threading.Tasks;
using SimpleInjector;

namespace ProofRegister.Common.Infrastructure.Commands
{
    public class CommandProcessor : ICommandProcessor
    {
        private readonly AutomaticCollection<Func<Task>> automaticCollection;
        private readonly Container container;
        private readonly ILogger logger;

        public CommandProcessor(Container container, ILogger logger, IExceptionHandling exceptionHandling)
        {
            this.container = container;
            this.logger = logger;
            automaticCollection = new AutomaticCollection<Func<Task>>("Commands", logger, AutomaticCollectionAction, exceptionHandling);
        }

        public void Enqueue<TCommand>(TCommand command, CancellationToken cancellationToken) where TCommand : ICommand
        {
            var handler = container.GetInstance<ICommandHandler<TCommand>>();
            var typeName = command.GetType().GetFriendlyName();
            Func<Task> action = async () =>
            {
                logger.WriteVerbose($"Start processing command {typeName} ID {command.CommandId} from queue.");
                await handler.HandleAsync(command, cancellationToken);
                logger.WriteVerbose($"Finished processing command {typeName} ID {command.CommandId} from queue.");
            };

            logger.WriteVerbose($"Enqueuing command {typeName} in {nameof(CommandProcessor)}.");
            automaticCollection.AddItem(action);
        }

        public async Task ProcessAsync<TCommand>(TCommand command, CancellationToken cancellationToken)
            where TCommand : ICommand
        {
            var handler = container.GetInstance<ICommandHandler<TCommand>>();

            var typeName = command.GetType().GetFriendlyName();
            try
            {
                logger.WriteVerbose($"Start processing command {typeName} ID {command.CommandId}.");
                await handler.HandleAsync(command, cancellationToken).ConfigureAwait(false);
                logger.WriteVerbose($"Finished processing command {typeName} ID {command.CommandId}.");
            }
            catch (OperationCanceledException)
            {
                logger.WriteWarning(
                    $"Processing command {typeName} ID {command.CommandId} has been cancelled.");
            }
        }

        private async Task AutomaticCollectionAction(Func<Task> action)
        {
            await action.Invoke();
        }
    }
}
