﻿using System.Threading;
using System.Threading.Tasks;

namespace ProofRegister.Common.Infrastructure.Commands
{
    public interface ICommandHandler<in TCommand> where TCommand : ICommand
    {
        Task HandleAsync(TCommand command, CancellationToken cancellationToken);
    }
}
