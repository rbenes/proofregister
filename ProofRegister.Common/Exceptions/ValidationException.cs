﻿using System;
using System.Collections.Generic;

namespace ProofRegister.Common.Exceptions
{
    public class ValidationException : Exception
    {
        public ICollection<string> Errors { get; private set; }
        

        public ValidationException(string message, ICollection<string> errors)
            : base(message)
        {
            this.Errors = errors;
        }
    }
}
