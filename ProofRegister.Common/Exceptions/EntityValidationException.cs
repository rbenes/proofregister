﻿using System.Collections.Generic;
using ProofRegister.Common.Messages;

namespace ProofRegister.Common.Exceptions
{
    public class EntityValidationException : ValidationException
    {
        private EntityValidationException(ICollection<string> errors)
            : base(ExceptionMessages.EntityValidationFailed, errors)
        {
        }
        
        public static EntityValidationException Create(ICollection<string> errors)
        {
            return new EntityValidationException(errors);
        }
    }
}
