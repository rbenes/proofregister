﻿using System;
using ProofRegister.Common.Infrastructure;

namespace ProofRegister.Common
{
    public class Logger : ILogger
    {
        public void Write(string message)
        {
            Console.WriteLine($"{DateTime.Now:s} - {message}");
        }

        public void WriteInfo(string message)
        {
            Write(message);
        }

        public void WriteVerbose(string message)
        {
            Write(message);
        }

        public void WriteWarning(string message)
        {
            Write(message);
        }

        public void WriteError(Exception exception)
        {
            Write($"Error: {exception}");
        }

        public void WriteStart(string message)
        {
            Write($"START - {message}");
        }

        public void WriteStop(string message)
        {
            Write($"STOP - {message}");
        }
    }
}
