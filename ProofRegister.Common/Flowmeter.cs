﻿using System;
using System.Timers;

namespace ProofRegister.Common
{
    public class Flowmeter : IDisposable
    {
        private readonly TimeSpan inverval;
        private readonly object syncRoot = new object();
        private readonly Timer timer;
        private long incomingItems;
        public event Action<decimal> OnInterval;

        public Flowmeter(TimeSpan inverval)
        {
            this.inverval = inverval;

            if (this.inverval.TotalSeconds <= 0)
                throw new ArgumentOutOfRangeException(nameof(inverval));
            
            incomingItems = 0;
            timer = new Timer { AutoReset = true, Enabled = true, Interval = this.inverval.TotalMilliseconds };
            timer.Elapsed += TimerOnElapsed;
        }

        public void Increment()
        {
            lock (syncRoot)
            {
                incomingItems++;
            }
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            decimal avgFlow;
            lock (syncRoot)
            {
                avgFlow = incomingItems / (decimal)inverval.TotalSeconds;
                incomingItems = 0;
            }
            OnInterval?.Invoke(avgFlow);
        }

        public void Dispose()
        {
            timer?.Dispose();
        }
    }
}
