﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using ProofRegister.Common.Infrastructure;

namespace ProofRegister.Common
{
    public class AutomaticCollection<T> : IDisposable
    {
        private readonly Flowmeter flowmeter;
        private readonly Func<T, Task> func;
        private readonly IExceptionHandling exceptionHandling;
        private readonly ILogger logger;
        private readonly string name;
        private readonly object syncRoot = new object();
        private readonly Task workingTask;

        public AutomaticCollection(string name, ILogger logger, Func<T, Task> func, IExceptionHandling exceptionHandling)
        {
            this.name = name;
            this.func = func;
            this.exceptionHandling = exceptionHandling;
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            Items = new BlockingCollection<T>();
            flowmeter = new Flowmeter(TimeSpan.FromMinutes(1));
            flowmeter.OnInterval += FlowmeterOnOnInterval;
            workingTask = Task.Run(async () => await ProcessItems());
        }

        public BlockingCollection<T> Items { get; set; }

        public TaskStatus Status => workingTask.Status;

        private bool IsAddingCompleted => Items?.IsAddingCompleted ?? true;

        public void Dispose()
        {
            flowmeter?.Dispose();
            workingTask?.Dispose();
            Items?.Dispose();
        }

        private void FlowmeterOnOnInterval(decimal value)
        {
            long count;
            lock (syncRoot)
            {
                count = Items?.Count ?? 0;
            }

            logger.WriteInfo($"[{name}] - Processing Summary [{count}|{value:F2}/s]");
        }

        public bool AddItem(T item)
        {
            lock (syncRoot)
            {
                if (IsAddingCompleted)
                    return false;

                Items.Add(item);
                flowmeter.Increment();
                return true;
            }
        }

        public void CompleteAdding()
        {
            lock (syncRoot)
            {
                Items?.CompleteAdding();
            }
        }

        private async Task ProcessItems()
        {
            try
            {
                logger.WriteInfo($"[{name}] - Process Started");

                foreach (var item in Items.GetConsumingEnumerable())
                    try
                    {
                        if (IsAddingCompleted)
                            return;

                        await func(item);
                    }
                    catch (Exception e)
                    {
                        exceptionHandling.HandleException(e);
                    }
            }
            catch(Exception e)
            {
                exceptionHandling.HandleException(e);
            }
            finally
            {
                logger.WriteInfo($"[{name}] - Process Finished");
            }
        }
    }
}
